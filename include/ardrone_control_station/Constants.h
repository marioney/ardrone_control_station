#ifndef CONSTANTS_H
#define CONSTANTS_H
#include "QString"
#include "QFont"
#define OPT_CAMERA 2 //defines the number of display types. Max.4
#define ALT_MAX 5000 //altura maxima en mm

//#define YAW_MAX 3//1.047196666 //300º/s o 180º/600ms [0.698132 , 6.108652]rad (40 , 350)º/s
//#define VZ_MAX 700//1000 //[200 , 2000]
//#define PITCH_MAX 0.261799388 //15º [0.087266 , 0.523599]rad (5 , 30)º


#define YAW_MAX 2//1.047196666 //300º/s o 180º/600ms [0.698132 , 6.108652]rad (40 , 350)º/s
#define VZ_MAX 700//1000 //[200 , 2000]
#define PITCH_MAX 0.13 //15º [0.087266 , 0.523599]rad (5 , 30)º

//#define NAV_DATA_OPT 67585
#define NAV_DATA_OPT 68097 //con lectura de pwm

#ifndef NUM_STR
    #define NUM_STR(x) QByteArray::number(x)
#endif

#define ARDRONE_NO_TRIM			\
    ((1 << ARDRONE_UI_BIT_TRIM_THETA) |	\
     (1 << ARDRONE_UI_BIT_TRIM_PHI) |	\
     (1 << ARDRONE_UI_BIT_TRIM_YAW) |	\
     (1 << ARDRONE_UI_BIT_X) |		\
     (1 << ARDRONE_UI_BIT_Y))

// Debug
//#define PRINT_INPUT_PER_SECOND
//#define SAVE_COMMANDS_LOG_FILE
//#define SAVE_MEASURES_LOG_FILE
#define USE_VIDEO_PROCESSOR
#define USE_JOYSTICK

/* Navdata constant */
#define NAVDATA_SEQUENCE_DEFAULT  1
#define NAVDATA_HEADER            0x55667788
#define FTP_PORT                  5551
#define NAVDATA_PORT              5554
#define AT_PORT                   5556
#define RAW_CAPTURE_PORT          5557
#define CONTROL_PORT              5559
#define WIFI_MYKONOS_IP           "192.168.1.1"

typedef struct _telemetry_t
{
    uint tmpBat;
    qint32 tmpAltitude;
    float tmpPhi;
    float tmpPsi;
    float tmpTheta;
    float vx;
    float vy;
    float vz;
    quint32 xc;
    quint32 yc;
    float ac;
    quint32 nb_detected;
    quint32   tag_width;
    quint32   tag_height;
    quint32   tag_dist;
    uint state;
    quint8     motor1;
    quint16    current_motor1;
    quint8     motor2;
    quint16    current_motor2;
    quint8     motor3;
    quint16    current_motor3;
    quint8     motor4;
    quint16    current_motor4;
} telemetry_t;



/*  NAD83	6,378,137.0	6,356,752.3141
    WGS84	6,378,137.0	6,356,752.3142
    Clark 1866	6,378,206.4	6,356,583.8
    Airy 1830	6,377,563.4	6,356,256.9*/

#define WGS84A 6378137.0
#define WGS84B 6356752.3142
//#define WGS84A 6378388.0
//#define WGS84B 6356911.946130
#define EX (sqrt((WGS84A*WGS84A) - (WGS84B*WGS84B)))/WGS84A //excentricidad
#define EX_ (sqrt((WGS84A*WGS84A) - (WGS84B*WGS84B)))/WGS84B//segunda excentricidad
#define EX_2 (EX_*EX_)
#define RP (WGS84A*WGS84A)/WGS84B //radio polar de curvatura c
#define ALFA (WGS84A-WGS84B)/WGS84A //Aplanamiento
//const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164;
#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164
#define RAD(x) ((x)*PI)/180

typedef struct _coodinate_t {
    double  lat;
    double  lon;
    double  x;
    double  y;
} coordinate_t;

typedef struct _wayPoint_t
{
    int ID;
    float yaw;
    coordinate_t coordinate;
    float altitude;
} wayPoint_t;
typedef struct _mission_t
{
    int wpID;
    int photo;
    int timeDelay;
    int err;
    wayPoint_t wayPoint;
} mission_t;
typedef struct _missionConf_t
{
    float yawErr,altErr,distWp,missionTout;
    coordinate_t boundMapIni,boundMapEnd;
} missionConf_t;

enum MSG_KEYS
{
    ID_MSG = 1,         //RMC protocol header
    UTC,                //UTC Time hhmmss.ss
    STATUS,             //A=data valid or V=data not valid
    LATITUDE,           //dddmm.mmmm
    LAT_ORIENTATION,    //N=north or S=south
    LONGITUDE,          //dddmm.mmmm
    LON_ORIENTATION,    //E=east or W=west
    MAG_VAR,            //Magnetic Variation in degrees
    M_V_ORIENTATION,    //E=east or W=west
    SPEED,              //Speed Over Ground in knots
    COURSE,             //Course Over Ground in degrees
    DATE,               //ddmmyy
    MODE,
    CHECKSUM            //checksum
};
const QString program = "iwconfig";
const QString LogHeader = "Time,UTM x,UTM y,latitude,longitude, altitude, roll[mdeg], yaw[mdeg], pitch[mdeg], batt[%], vx[mm/s], vy[mm/s], vz[mm/s],detected,tagX,tagY,tagAngle,tagDist,tagheight,tagwidth";
const int mapWidthPix=560;
const int mapHeightPix=420;
#endif // CONSTANTS_H
