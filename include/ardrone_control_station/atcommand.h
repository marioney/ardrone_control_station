#ifndef ATCOMMAND_H
#define ATCOMMAND_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QUdpSocket>
#include <QFile>
#include <QHostAddress>
#include <QUdpSocket>
#include <QByteArray>
#include <QTimer>
#include <QDateTime>
#include <QDataStream>
#include <QTcpSocket>


#include "navdata.h"

#define OPT_CAMERA 2 //defines the number of display types. Max.4
#define ALT_MAX 5000 //altura maxima en mm

//#define YAW_MAX 3//1.047196666 //300º/s o 180º/600ms [0.698132 , 6.108652]rad (40 , 350)º/s
//#define VZ_MAX 700//1000 //[200 , 2000]
//#define PITCH_MAX 0.261799388 //15º [0.087266 , 0.523599]rad (5 , 30)º


#define YAW_MAX 2//1.047196666 //300º/s o 180º/600ms [0.698132 , 6.108652]rad (40 , 350)º/s
#define VZ_MAX 700//1000 //[200 , 2000]
#define PITCH_MAX 0.13 //15º [0.087266 , 0.523599]rad (5 , 30)º

//#define NAV_DATA_OPT 67585
#define NAV_DATA_OPT 68097 //con lectura de pwm



class QTimer;

#ifndef NUM_STR
    #define NUM_STR(x) QByteArray::number(x)
#endif

#define ARDRONE_NO_TRIM			\
    ((1 << ARDRONE_UI_BIT_TRIM_THETA) |	\
     (1 << ARDRONE_UI_BIT_TRIM_PHI) |	\
     (1 << ARDRONE_UI_BIT_TRIM_YAW) |	\
     (1 << ARDRONE_UI_BIT_X) |		\
     (1 << ARDRONE_UI_BIT_Y))

// Debug
//#define PRINT_INPUT_PER_SECOND
//#define SAVE_COMMANDS_LOG_FILE


class AtCommand : public QThread
{
    Q_OBJECT

public:
    AtCommand(QObject *parent = 0);
    ~AtCommand();

    void startThread();

protected:
    void run();

public Q_SLOTS:
    void setAxis(qreal phi, qreal theta);
    void setData(qreal gaz, qreal yaw);
    void changeCamera();
    void updateARDroneState(uint state);
    void emergencyPressed();
    void startPressed();
    void sendInitSequence(int step);
    void detectTag(int control);

private Q_SLOTS:
    void flatTrim();
#ifdef PRINT_INPUT_PER_SECOND
    void handleIPSTimer();
#endif
Q_SIGNALS:
//    void command(QByteArray);

private:
    QMutex mMutex;
    QWaitCondition mCondition;
    bool mAbort;
    uint mARDroneState;
    int mPhi;
    int mTheta;
    int mGaz;
    int mYaw;
    bool mCameraChange;
    uint mUserInput;
    uint InitnbSeq;
    bool fTrim, detectT;
    QByteArray OutShell, OutFlight;
#ifdef PRINT_INPUT_PER_SECOND
    QTimer *mIpsTimer;
    int mIpsMovCounter;
    int mIpsDataCounter;
    int mIpsSendCounter;
#endif
#ifdef SAVE_COMMANDS_LOG_FILE
    QFile measurementLog;
    QByteArray lastCommand;
#endif
    float_or_int_t _gaz,_yaw,_phi, _theta;
    int controlT;

public:
    bool flag_config;

};

#endif // ATCOMMAND_H
