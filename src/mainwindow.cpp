#include <ui_main.h>
#include <ardrone_control_station/mainwindow.h>

#include <ardrone_control_station/navdatahandler.h>
#include <ardrone_control_station/atcommand.h>
#include <ardrone_control_station/settings.h>
#include <ardrone_control_station/missioncontrol.h>
#include <ui_settings.h>
#ifdef USE_VIDEO_PROCESSOR
#include <ardrone_control_station/videoprocessor.h>
#endif

/*!
    Constructs the \c MainWindow.
    The \a parent parameter is sent to the QMainWindow constructor.
*/
#ifdef USE_ROS_NODE
MainWindow::MainWindow(int argc, char** argv,QWidget *parent) : QMainWindow(parent)
        ,ui(new Ui::MainWindow),qnode(argc,argv)
#else
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
        ui(new Ui::MainWindow)
#endif
{

//    this->thread()->setPriority(QThread::HighPriority);
    userName="noName";
    mLoadDots =0;
    mConnectedToDrone = false;
    qDebug() << "[MainWindow] Constructor";
    mNoConnectionExit = true;
    msettings = new Settings(this);
    ui->setupUi(this);

    QString program = "iwconfig";
         QStringList arguments;

         myProcess = new QProcess(this);
         myProcess->start(program);
         connect(myProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(iwconfig()));


    ui->tabWidget->removeTab(1); // esconde el tab auxiliar
    ui->tabWidget->removeTab(1); // esconde el tab auxiliar
    ui->tabWidget->removeTab(1); // esconde el tab auxiliar

    // Create a scene to place the background image on and add it to the
    // graphicsView object in the UI.
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    lastImage = QImage(); //al inicio es una imagen nula
    imgDirPath = "./Photos";
    msettings->ui->imgDirectoryLEdit->setText(imgDirPath);
    terrain= new QPixmap(640,480);
    sceneMap= new QGraphicsScene(this);
    ui->mapView->setScene(sceneMap);


    lastState=0;
    icono = new QIcon(QPixmap(":/images/TalvikkiIcon.png"));
    msgBox.setWindowIcon(*icono);

// inicializacion del puerto
    portName = "/dev/ttyUSB0";
    msettings->ui->portNameLEdit->setText(portName);
    port =  NULL;



    homeCoord.lat=0;
    homeCoord.lon=0;
    homeCoord.x=0;
    homeCoord.y=0;
    currCoord.lat=0;
    currCoord.lon=0;
    currCoord.x=0;
    currCoord.y=0;
    //homeSet=false;
//    testCoordinates("4026.35944","N","00341.30790","W");
//    testCoordinates("4026.35944","N","00341.29032","W");
//    testCoordinates("4026.36958","N","00341.30790","W");
//    testCoordinates("4026.36958","N","00341.29032","W");

/******************************************************************************************************
    inicialización componentes qwt
*******************************************************************************************************/
    ui->Compass->setScaleComponents(
        QwtAbstractScaleDraw::Ticks | QwtAbstractScaleDraw::Labels);
    ui->Compass->setScaleTicks(1, 1, 3);
    ui->Compass->setScale(36, 5, 0);
    ui->Compass->setNeedle(new QwtCompassMagnetNeedle(QwtCompassMagnetNeedle::ThinStyle));

    ui->Speedmeter->setRange(0.0,5.0);
    ui->Speedmeter->setScale(20,50,0);
    ui->Speedmeter->setOrigin(135.0);
    ui->Speedmeter->setScaleArc(0.0, 270.0);
    ui->Speedmeter->scaleDraw()->setSpacing(5);
    QwtDialSimpleNeedle *SpeedNeedle = new QwtDialSimpleNeedle(
            QwtDialSimpleNeedle::Arrow, true, Qt::red,
            QColor(Qt::gray).light(130));
    ui->Speedmeter->setNeedle(SpeedNeedle);
    ui->Speedmeter->setScaleComponents(
        QwtAbstractScaleDraw::Ticks | QwtAbstractScaleDraw::Labels);
    ui->Speedmeter->setScaleTicks(0, 4, 8);

    ui->altitudeInd->setRange(0.0,5000.0);
    ui->altitudeInd->setScale(20,50,0);
    ui->altitudeInd->setOrigin(135.0);
    ui->altitudeInd->setScaleArc(0.0, 270.0);
    ui->altitudeInd->scaleDraw()->setSpacing(5);
    QwtDialSimpleNeedle *AltNeedle = new QwtDialSimpleNeedle(
            QwtDialSimpleNeedle::Arrow, true, Qt::green,
            QColor(Qt::gray).light(130));
    ui->altitudeInd->setNeedle(AltNeedle);
    ui->altitudeInd->setScaleComponents(
        QwtAbstractScaleDraw::Ticks | QwtAbstractScaleDraw::Labels);
    ui->altitudeInd->setScaleTicks(0, 5, 10);

    test = new AttitudeIndicator(ui->frame_2);
    test->setGeometry(QRect(2, 18, 200, 200));
    test->setReadOnly(true);
    test->scaleDraw()->setPenWidth(3);
    test->setLineWidth(4);
    test->setFrameShadow(QwtDial::Sunken);

    // Hide the buttons until they are needed.
    ui->labelStatus->setVisible(false);
    ui->emergencyBox->setVisible(false);
    ui->emergencyOnVideo->setVisible(false);
//    ui->TelemetryGBox->setVisible(false);
//    ui->tabWidget->removeTab(2); // esconde el tab auxiliar
    mConnectState = 0;
    ui->ardroneConnectBtn->setText("Connect");
    correccionPsi=0;
    takePsiOffset=false;
    isMissionLoaded=false;
    paintFirstTime=false;
    newGpsData=false;
    isHomesetted=false;
    msettings->ui->imgDirectoryLEdit->setText(imgDirPath);
    msettings->ui->portNameLEdit->setText(portName);
    msettings->show();
    ui->startMissionBtn->setEnabled(false);
    ui->startMissionBtn_2->setEnabled(false);
    ui->clearMissionBtn->setEnabled(false);

    tcpSocketCmd = new QTcpSocket(this);
    connect(tcpSocketCmd,SIGNAL(readyRead()),this,SLOT(newConfDataReady()));
    recFrames=false;
//    ui->lcdNumber

}
void MainWindow::iwconfig()
{
  qDebug()<<myProcess->readAllStandardOutput();
}

/*!
    Destructor.
*/
MainWindow::~MainWindow()
{
    qDebug() << "[MainWindow] Destructor";
    // If we are exiting before we got a connection to the drone then the following components will
    // not have been loaded yet.
    if (!mNoConnectionExit) {
        noDroneConnected();
    }
    delete ui;
}


/*!
    Slot that will load all the different components and set up signals between them once the
	Loader signals that connection to the drone has been confirmed.
*/
void MainWindow::droneConnected(QString version)
{
    qDebug() << "[MainWindow] Connected to drone. Firmware version: " << version.mid(0,5);
    mNavDataHandler = new NavDataHandler();    
    mAtCommand = new AtCommand();
#ifdef USE_VIDEO_PROCESSOR
    mVideoProcessor = new VideoProcessor();
    qRegisterMetaType<QImage>("QImage");
    connect(mVideoProcessor, SIGNAL(newImageProcessed(QImage*)),
            this, SLOT(recieveVideoImage(QImage *)));
#endif

    connect(mNavDataHandler, SIGNAL(updateARDroneState(uint)),
            mAtCommand, SLOT(updateARDroneState(uint)));
    connect(mNavDataHandler, SIGNAL(updateEmergencyState(bool)),
            this, SLOT(updateEmergencyState(bool)));
    connect(mNavDataHandler, SIGNAL(updateNavdata(telemetry_t *)),
            this, SLOT(updateNavdata(telemetry_t *)));
#ifdef USE_ROS_NODE
    connect(this, SIGNAL(PublishImg(QImage)),
            &qnode, SLOT(rcvImage(QImage)));
    connect(this, SIGNAL(publishNavdata(telemetry_t *, coordinate_t *)),
            &qnode, SLOT(publishNavdata(telemetry_t *, coordinate_t *)));
#endif

    connect(mNavDataHandler, SIGNAL(sendInitSequence(int)),
            mAtCommand, SLOT(sendInitSequence(int)));
    connect(this,SIGNAL(detectTag(int)),mAtCommand,SLOT(detectTag(int)));
    /************************************************************************************************************/
    mJoystick = new Joystick(0);
    connect(mJoystick,SIGNAL(sendData(qreal,qreal)),mAtCommand,SLOT(setData(qreal,qreal)));
    connect(mJoystick,SIGNAL(sendAxis(qreal,qreal)),mAtCommand,SLOT(setAxis(qreal,qreal)));
    connect(mJoystick,SIGNAL(changeCamera()),mAtCommand,SLOT(changeCamera()));
    connect(mJoystick,SIGNAL(emergencyPressed()),mAtCommand,SLOT(emergencyPressed()));
    connect(mJoystick,SIGNAL(startPressed()),mAtCommand,SLOT(startPressed()));
    connect(mJoystick,SIGNAL(saveImg()),this,SLOT(saveImg()));

    /***********************************************************************************************************/
//    mMissionCtrl = new missionCtrl(mAtCommand,this);
//    connect(mJoystick, SIGNAL(buttonPress(int)), mMissionCtrl, SLOT(abortMission(int)));
//    connect(mNavDataHandler, SIGNAL(updateNavdata(telemetry_t *)),
//            mMissionCtrl, SLOT(getTelemetry(telemetry_t*)));
//    connect(mMissionCtrl,SIGNAL(sendData(qreal,qreal)),mAtCommand,SLOT(setData(qreal,qreal)));
//    connect(mMissionCtrl,SIGNAL(sendAxis(qreal,qreal)),mAtCommand,SLOT(setAxis(qreal,qreal)));
//    connect(mMissionCtrl,SIGNAL(changeCamera()),mAtCommand,SLOT(changeCamera()));
//    connect(mMissionCtrl,SIGNAL(emergencyPressed()),mAtCommand,SLOT(emergencyPressed()));
//    connect(mMissionCtrl,SIGNAL(startPressed()),mAtCommand,SLOT(startPressed()));
//    connect(mMissionCtrl,SIGNAL(stopPressed()),mAtCommand,SLOT(stopPressed()));
//    connect(this, SIGNAL(mission(QList<mission_t>*,missionConf_t*)),
//            mMissionCtrl, SLOT(getMission(QList<mission_t>*,missionConf_t*)));
//    connect(this, SIGNAL(sendLocation(coordinate_t*)),
//            mMissionCtrl, SLOT(getCurrLocation(coordinate_t*)));
//    connect(this,SIGNAL(camVert(bool)),mMissionCtrl,SLOT(camVert(bool)));
//    connect(mMissionCtrl, SIGNAL(saveImg()),this,SLOT(saveImg()));
//    connect(mMissionCtrl, SIGNAL(showCurrentStep(int)),this, SLOT(showCurrentStep(int)));

    /**********************************************************************************************************/

    mainComThread = new QMainComThread(portName);
    qDebug()<<"CREA EL JOYSTICK";
    mainComThread->start();
    while (!port)
    {
        port = mainComThread->getPort();
        qApp->processEvents();
    }
    port->setPortName(portName);
    port->open(QIODevice::ReadWrite);
    if (port->isOpen())
        qDebug()<<"Port open";
    else
        qDebug()<<"Port not open";

//    connect(port,SIGNAL(newDataInPortSignal(QTime,const unsigned char *, const int)),
//            this, SLOT(receiveMsg(QTime,const unsigned char *, const int)));
    qRegisterMetaType<coordinate_t>("coordinate_t");
    connect(port,SIGNAL(newDataInPortSignal(coordinate_t *)),
            this, SLOT(receiveMsg(coordinate_t *)));
    /**********************************************************************************************************/

    mAtCommand->startThread();
    mNoConnectionExit = false;
}

void MainWindow::noDroneConnected()
{
    delete mNavDataHandler;
    delete mAtCommand;
#ifdef USE_VIDEO_PROCESSOR
    delete mVideoProcessor;
#endif

    delete mJoystick;
//    port->close();
//    delete mMissionCtrl;
    mNoConnectionExit = true;

}

/*!
    Changes the text of the status label in the UI.
    HTML formatting is added around the parameter \a text, before it is displayed.
*/
void MainWindow::setStatusLabelText(QString text)
{
    ui->labelStatus->setText(
            QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\""
                              " \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                              "<html><head><meta name=\"qrichtext\" content=\"1\" /><style "
                              "type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style>"
                              "</head><body style=\" font-family:'MS Shell Dlg 2'; "
                              "font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                              "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; "
                              "margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span "
                              "style=\" font-size:6pt; color:#000000;\">")
            + text
            + QString::fromUtf8("</span></p></body></html>"));
}

/*!
    Changes the text of the error label in the UI.
    HTML formatting is added around the parameter \a text, before it is displayed. The parameter
    \a size is the font size used. If \a size is not set a default value of 10 will be used.
*/
QString MainWindow::setErrorLabelText(QString text, QString size)
{
    return QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\""
                                  " \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                  "<html><head><meta name=\"qrichtext\" content=\"1\" /><style "
                                  "type=\"text/css\">\np, li { white-space: pre-wrap; }\n"
                                  "</style></head><body style=\" font-family:'MS Shell Dlg 2'; "
                                  "font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                  "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; "
                                  "margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span "
                                  "style=\" font-size:")
                + size
                + QString::fromUtf8("pt; color:#000000;\">")
                + text
                + QString::fromUtf8("</span></p></body></html>");
}

/*!
    The loader timer updates the status message and indicates loading activity to the user.
*/
void MainWindow::handleLoaderTimer()
{
    mLoadDots++;
    mLoadDots = (mLoadDots%30) + 1;
    QString loadDotString = "";

    if (0 == mConnectState) {
        loadDotString  = "Searching for AR.Drone<br />";
    } else if (1 == mConnectState) {
        loadDotString = "WLAN connection found. Connecting to AR.Drone<br />";
    }

    for (int i = 0; i < mLoadDots; i++) {
        loadDotString += ".";
    }
    setStatusLabelText(loadDotString);
    update();
}

/*!
    Looks up the AR.Drone host to check if we are connected to the drone.
*/
void MainWindow::lookUpHost()
{
    QHostInfo::lookupHost(WIFI_MYKONOS_IP, this, SLOT(lookedUp(QHostInfo)));
}

/*!
    This function receives the host information from the host look up.
    The parameter \a host contains the information on the host to which we are connected.
*/
void MainWindow::lookedUp(const QHostInfo &host)
{
//    if (host.error() != QHostInfo::NoError) {
//        qDebug() << "Lookup failed:" << host.errorString();
//        return;
//    }

//    foreach (const QHostAddress &address, host.addresses())
//        qDebug() << "Found address:" << address.toString();
    if (host.localHostName() == "") {
        qDebug() << "[MainWindow] Host lookup failed:" << host.errorString();
        mLoaderTimer->stop();
        ui->labelStatus->setVisible(false);
        msgBox.setText(setErrorLabelText("Unable to connect to the AR.Drone.<br />Make sure the AR.Drone "
                          "is powered and that the central computer is connected to the Drone "
                          "through WLAN. Then please restart the application.<br /><br />"
                          "Note that you need an AR.Drone quadrocopter for this app to be of any "
                          "use. See http://ardrone.parrot.com"
                          , "8"));
        msgBox.exec();
        mConnectState = 0;


    } else {

        mFtp = new QFtp(this);
        mFtp->connectToHost(WIFI_MYKONOS_IP, FTP_PORT);
        connect(mFtp, SIGNAL(readyRead()),this, SLOT(readDroneFtp()));
        mFtp->login("anonymous");
        mFtp->get("version.txt");
        mFtp->close();
        // Set a timeout timer in case FTP get fails.
        QTimer::singleShot(5000, this, SLOT(droneConnectionTimeout()));

    }
}

/*!
    This function is called when the FTP get returns with version.txt.
*/
void MainWindow::readDroneFtp()
{
    // FTP answer from the drone. This means we are connected!
    mConnectedToDrone = true;
    ui->labelStatus->setVisible(false);
//    ui->TelemetryGBox->setVisible(true);
    ui->ardroneConnectBtn->setText("Disconnect");
    mVersion = mFtp->readAll();
    droneConnected(mVersion);
    ui->graphicsView->setVisible(true);
    mConnectState = 1;
}

/*!
    This function is called if the FTP get takes too long answering.
*/
void MainWindow::droneConnectionTimeout()
{
    // Check if we have a connection. Maybe the user is just waiting a long time before pressing
    // a button.
    if (!mConnectedToDrone) {
        qDebug() << "[MainWindow] FTP to drone timed out.";
        mLoaderTimer->stop();
        ui->labelStatus->setVisible(false);
        msgBox.setText(setErrorLabelText("Unable to retrieve version information from the AR.Drone.<br />Make sure"
                          " the AR.Drone is powered and that central computer is connected to the Drone "
                          "through WLAN. Then please restart the application.<br /><br />"
                          "Note that you need an AR.Drone quadrocopter for this app to be of any "
                          "use. See http://ardrone.parrot.com"
                          , "8"));
        msgBox.exec();

    }
}

/*!
    Slot for receiving a new image from the video feed.
    The parameter \a image contains the new image.
*/
void MainWindow::recieveVideoImage(QImage *image)
{
    if(image->isNull()){
        return;
    }
    if (144 == image->height())
    {
        *image=image->copy(0,0,176,144);
//        ui->graphicsView->setGeometry(QRect(10,20,176,144));
        Q_EMIT camVert(true);
    }else{
//        ui->graphicsView->setGeometry(QRect(10,20,320,180));
        Q_EMIT camVert(false);
    }
//    if (1 <= scene->items().size())
//    {
//        scene->clear();
//    }
#ifdef USE_ROS_NODE
    Q_EMIT PublishImg(*image);
#endif
    scene->clear();
    /****modo pixmap****/
//    QPixmap *img = new QPixmap(); // se crea temporalmente para incluirla en la escenea
//    *img=img->fromImage(*image);
//    if(img->isNull()==false){
////        scene->addPixmap(img->scaled(QSize::QSize ( 480, 270 )));
//        scene->addPixmap(img->scaled(QSize::QSize ( 320, 180 )));

//    }

//    scene->addPixmap(QPixmap::fromImage(*image).scaled(QSize::QSize ( 480, 270 )));
//    ui->graphicsView->setScene(scene);
    /****modo background****/
        scene->setBackgroundBrush(QBrush::QBrush(image->scaled(QSize::QSize ( 480, 270 )))) ;//no hace conversion a pixmap
    /***********************/
    ui->graphicsView->update();
//    delete img; //libera memoria temporal (memory leak)
    if(recFrames){
        if (imgDirPath.isEmpty()) imgDirPath=".";
        QString test = imgDirPath +"/"+ QDateTime::currentDateTime().toString("hh:mm:ss.zzz")+".png";
        image->save(test,0,-1);
    }else{
        // se guarda la ultima imagen para para cuando se requiera guardar
        lastImage = *image;
    }

}

void MainWindow::updateNavdata(telemetry_t *telemetry)
{
    Q_EMIT publishNavdata(telemetry, &currCoord);
    ui->vXValue->setText(QString::number(telemetry->vx,'f',3));
    ui->vYValue->setText(QString::number(telemetry->vy,'f',3));
    ui->vZValue->setText(QString::number(telemetry->vz,'f',3));
    ui->vXValue_2->setText(QString::number(telemetry->vx/1000,'f',3));
//    ui->Speedmeter->setValue(qSqrt((qPow(telemetry->vx,2) + qPow(telemetry->vy,2)))/1000);
    ui->Speedmeter->setValue(telemetry->vx/1000);
//    correccionPsi=0;
//    qDebug()<<"antes"<<QString::number(telemetry->tmpPsi/1000,'f',3);
//    if(!takePsiOffset){
//        correccionPsi=telemetry->tmpPsi;
//        takePsiOffset=true;
//    }
//    telemetry->tmpPsi = telemetry->tmpPsi - correccionPsi;
//    qDebug()<<"despues"<<QString::number(telemetry->tmpPsi/1000,'f',3);
    ui->yawValue->setText(QString::number(telemetry->tmpPsi/1000,'f',3) + QChar(0x00BA));
    ui->rollValue->setText(QString::number(telemetry->tmpPhi/1000,'f',3) + QChar(0x00BA));
    ui->pitchValue->setText(QString::number(telemetry->tmpTheta/1000,'f',3) + QChar(0x00BA));
    ui->batteryMeter->setValue(telemetry->tmpBat);
//    QString test1 = QString::number(telemetry->xc)+","
//            +QString::number(telemetry->yc)+","
//            +QString::number(telemetry->ac)+","
//            +QString::number(telemetry->tag_dist)+","
//            +QString::number(telemetry->tag_height)+","
//            +QString::number(telemetry->tag_width)+","
//            +QString::number(telemetry->nb_detected);
    ui->tagDetectedValue->setText(QString::number(telemetry->nb_detected));
    ui->tagXValue->setText(QString::number(telemetry->xc));
    ui->tagYValue->setText(QString::number(telemetry->yc));
    ui->tagAngleValue->setText(QString::number(telemetry->ac));
    ui->tagDistValue->setText(QString::number(telemetry->tag_dist));
    ui->tagHeightValue->setText(QString::number(telemetry->tag_height));
    ui->tagWidthValue->setText(QString::number(telemetry->tag_width));
//    ui->tmpValue->setText(test1);
//    qDebug()<< "[AR Drone]"+ test1;
    QString Altitude = (telemetry->tmpAltitude>220)?
                QString::number(1.13*telemetry->tmpAltitude):
                "<" + QString::number(telemetry->tmpAltitude);
    ui->altitudeValue->setText(Altitude);
    ui->altitudeInd->setValue(telemetry->tmpAltitude);
    ui->Compass->setValue(telemetry->tmpPsi/1000);
    test->setAngle(telemetry->tmpPhi/-1000);
    test->setGradient(telemetry->tmpTheta/1000/90);
    if(lastState != telemetry->state)
    {
        lastState = telemetry->state;
        ui->flystatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_FLY_MASK),
                                                   "Flying","Landed","Green","Blue"));
        ui->startStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_USER_FEEDBACK_START),
                                                   "Started","Stopped","Green","Blue"));
        ui->navDataValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_NAVDATA_DEMO_MASK),
                                                   "Demo","All","Green","Blue"));
        ui->motorStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_MOTORS_MASK),
                                                   "Motors Problem","OK","Red","Green"));
        ui->commStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_COM_LOST_MASK),
                                                   "Communication Problem","Ok","Red","Green"));
        ui->battLowValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_VBAT_LOW),
                                                   "Battery to low","Ok","Red","Green"));
        ui->powerFlyValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_POWER_FLY_MASK),
                                                   "Not enough to fly","Ok","Red","Green"));
        ui->anglesStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_ANGLES_OUT_OF_RANGE),
                                                   "Out of range","Ok","Red","Green"));
        ui->windStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_WIND_MASK),
                                                   "Too much to fly","OK","Red","Green"));
        ui->uSonicSensStatValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_ULTRASOUND_MASK),
                                                   "Deaf","Ok","Red","Green"));
        ui->commWDogValue->setText(formatStateLabel(get_mask_from_state(telemetry->state, ARDRONE_COM_WATCHDOG_MASK),
                                                   "Communication Problem","Ok","Red","Green"));
    }
#ifdef SAVE_MEASURES_LOG_FILE
    QTextStream measuresTxt(&measurementLog);
    if(measurementLog.size() <= 0)
    {
        measurementLog.open(QIODevice::WriteOnly | QIODevice::Text);
//        measuresTxt << "time,UTM x,UTM y,latitude,longitude, altitude, roll[mdeg], yaw[mdeg], pitch[mdeg], bat[%], vx[mm/s], vy[mm/s], vz[mm/s]\n";
        measuresTxt << "time,detected,tagX,tagY,tagAngle,tagDist,tagheight,tagwidth, altitude, roll[mdeg], yaw[mdeg], pitch[mdeg], bat[%], vx[mm/s], vy[mm/s], vz[mm/s]\n";

    }else
    {
//        measuresTxt << QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + ";"
//                + QString::number(currCoord.x,'f',5) + ";"
//                + QString::number(currCoord.y,'f',5) + ";"
//                + QString::number(currCoord.lat,'f',5) + ";"
//                + QString::number(currCoord.lon,'f',5) + ";"
//                + QString::number(telemetry->tmpAltitude,'f',5)+";"
//                + QString::number(telemetry->tmpPhi,'f',5)+";"
//                + QString::number(telemetry->tmpPsi,'f',5)+";"
//                + QString::number(telemetry->tmpTheta,'f',5)+";"
//                + QString::number(telemetry->tmpBat,'f',3)+";"
//                + QString::number(telemetry->vx,'f',5)+";"
//                + QString::number(telemetry->vy,'f',5)+";"
//                + QString::number(telemetry->vz,'f',5)+";"
//                +"\n";
        measuresTxt << QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + ";"
                + QString::number(telemetry->nb_detected,'f',5) + ";"
                + QString::number(telemetry->xc,'f',5) + ";"
                + QString::number(telemetry->yc,'f',5) + ";"
                + QString::number(telemetry->ac,'f',5) + ";"
                + QString::number(telemetry->tag_dist,'f',5) + ";"
                + QString::number(telemetry->tag_height,'f',5) + ";"
                + QString::number(telemetry->tag_width,'f',5) + ";"
                + QString::number(telemetry->tmpAltitude,'f',5)+";"
                + QString::number(telemetry->tmpPhi,'f',5)+";"
                + QString::number(telemetry->tmpPsi,'f',5)+";"
                + QString::number(telemetry->tmpTheta,'f',5)+";"
                + QString::number(telemetry->tmpBat,'f',3)+";"
                + QString::number(telemetry->vx,'f',5)+";"
                + QString::number(telemetry->vy,'f',5)+";"
                + QString::number(telemetry->vz,'f',5)+";"
                +"\n";
    }
#endif
    //si el ARDrone está volando, deshabilita el FTrim
    if(get_mask_from_state(telemetry->state, ARDRONE_FLY_MASK)==1)
        ui->FTrimBtn->setEnabled(false);
    else
        ui->FTrimBtn->setEnabled(true);

}

QString MainWindow::formatStateLabel(int state,QString text0,QString text1,QString color0,QString color1)
{
    QString texto;
    if(state)
    {
        texto = "<font color='"+ color0 +"'>"+ text0 +"</font>";
    }
    else{
        texto = "<font color='"+ color1 +"'>"+ text1 +"</font>";
    }
    return texto;
}
/*!
    Slot to receive updated emergency status from \c NavDataHandler.
    The parameter \a on tells wether emergency state is active or not.
*/
void MainWindow::updateEmergencyState(bool on)
{
    ui->emergencyBox->setVisible(on);
    ui->emergencyOnVideo->setVisible(on);
}
void MainWindow::receiveMsg(coordinate_t * curr)
{
    mMutex.lock();
    currCoord=*curr;
    mMutex.unlock();
    calculateUTM(&currCoord);
//    qDebug()<<currCoord.lat<<currCoord.lon<<currCoord.x<<currCoord.y;
    Q_EMIT sendLocation(&currCoord);
    newGpsData=true;
    update();
//    ui->timeUTCValue->setText(msg.value(UTC));
    ui->longitudeValue->setText(QString::number(currCoord.lon, 'f', 6));
    ui->laltitudeValue->setText(QString::number(currCoord.lat, 'f', 6));
    ui->xUTMValue->setText(QString::number(currCoord.x, 'f', 6));
    ui->yUTMValue->setText(QString::number(currCoord.y, 'f', 6));
    if(!homeSet)
    {
        homeCoord=currCoord;
        ui->homeLatValue->setText(QString::number(homeCoord.lat, 'f', 6));
        ui->homeLonValue->setText(QString::number(homeCoord.lon, 'f', 6));
        ui->homeXValue->setText(QString::number(homeCoord.x, 'f', 6));
        ui->homeYValue->setText(QString::number(homeCoord.y, 'f', 6));
        homeSet=true;
    }


}
//void MainWindow::receiveMsg(const QTime timesl, const unsigned char *data, const int size)
//{
////    qDebug()<<"Size:  "<<size<<" "<<timesl;
//    QHash<int,QString> msg;
//    QTime timedb;
//    timedb = timesl ;
//    int c=1;
//    QString tmp;
//    int i = 0;
//    for (;i < size;i++)
//    {
//        switch (data[i])
//        {
//            case '\x2C': //,
//                msg.insert(c,tmp);
//                c++;
//                tmp.clear();
//                break;
//            case '\x2A': //*
//                msg.insert(c,tmp);
//                tmp.clear();
//                tmp.append(data[i+1]);
//                tmp.append(data[i+2]);
//                msg.insert(CHECKSUM,tmp);
//                tmp.clear();
//                c=1;
//                break;
//            default:
//                tmp += data[i];
//        }
//    }
//    if(msg.value(STATUS)=="A")
//    {
////        testCoordinates(msg.value(LATITUDE), msg.value(LAT_ORIENTATION),msg.value(LONGITUDE), msg.value(LON_ORIENTATION));
//        double lat =msg.value(LATITUDE).mid(0,2).toDouble()+
//                    msg.value(LATITUDE).mid(2).toDouble()/60;
//        if(msg.value(LAT_ORIENTATION)=="S") lat*=-1;

//        double lon =msg.value(LONGITUDE).mid(0,3).toDouble()+
//                    msg.value(LONGITUDE).mid(3).toDouble()/60;
//        if(msg.value(LON_ORIENTATION)=="W") lon*=-1;

//        currCoord.lat=lat;
//        currCoord.lon=lon;
//        calculateUTM(&currCoord);
//        emit sendLocation(&currCoord);
//        newGpsData=true;
//        update();
//        ui->timeUTCValue->setText(msg.value(UTC));
//        ui->longitudeValue->setText(QString::number(currCoord.lon, 'f', 6));
//        ui->laltitudeValue->setText(QString::number(currCoord.lat, 'f', 6));
//        ui->xUTMValue->setText(QString::number(currCoord.x, 'f', 6));
//        ui->yUTMValue->setText(QString::number(currCoord.y, 'f', 6));
//        if(!homeSet)
//        {
//            homeCoord=currCoord;
//            ui->homeLatValue->setText(QString::number(homeCoord.lat, 'f', 6));
//            ui->homeLonValue->setText(QString::number(homeCoord.lon, 'f', 6));
//            ui->homeXValue->setText(QString::number(homeCoord.x, 'f', 6));
//            ui->homeYValue->setText(QString::number(homeCoord.y, 'f', 6));
//            homeSet=true;
//        }

//    }else
//    {
//        QString errMsg= "wrong GPS data";
////        qDebug()<<errMsg<< msg.value(UTC);
//        ui->timeUTCValue->setText("<font color='red'>"+ errMsg +"</font>");
//        ui->longitudeValue->setText("<font color='red'>"+ errMsg +"</font>");
//        ui->laltitudeValue->setText("<font color='red'>"+ errMsg +"</font>");
//        ui->xUTMValue->setText("<font color='red'>"+ errMsg +"</font>");
//        ui->yUTMValue->setText("<font color='red'>"+ errMsg +"</font>");
//    }
////    ui->textBrowser->append(msg.values().at(1));
//}

void MainWindow::on_settingsButton_clicked()
{
    msettings->show();
}

void MainWindow::on_ardroneConnectBtn_clicked()
{
    if (mConnectState == 0)
    {
        setStatusLabelText("Searching for AR.Drone<br />.");
        ui->labelStatus->setVisible(true);
        mLoaderTimer = new QTimer(this);
        connect(mLoaderTimer, SIGNAL(timeout()), this, SLOT(handleLoaderTimer()));
        mLoaderTimer->start(75);
//        lookUpHost();
        QTimer::singleShot(1, this, SLOT(lookUpHost()));
#ifdef USE_ROS_NODE
        qnode.init();
#endif
    }
    else
    {
        mFtp->close();

        noDroneConnected();
        ui->ardroneConnectBtn->setText("Connect");
//        ui->TelemetryGBox->setVisible(false);
        mConnectState = 0;
    }
}

void MainWindow::on_saveImageBtn_clicked()
{
    saveImg();
}
void MainWindow::saveImg()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTimeString = dateTime.toString() +".png";
    if (imgDirPath.isEmpty()) imgDirPath=".";
    QString test = imgDirPath +"/1"+userName+ dateTimeString;
    qDebug()<<test;
        if (lastImage.save(test,0,-1))
    {
        qDebug()<<test;
      /*  msgBox.setText("La imagen ha sido guardada con el nombre:\n" + dateTimeString);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();*/
    }/**/
    else
    {
        msgBox.setText("La imagen no se ha guardado.\nDirectorio: " + test);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }

}

void MainWindow::on_imgDirectoryBtn_clicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this,
                                                         tr("Directory to save photos"),
                                                         "/home/david/ardrone/Photos",
                                                         QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    msettings->ui->imgDirectoryLEdit->setText(fileName);
}

void MainWindow::on_buttonBox_accepted()
{
    imgDirPath = msettings->ui->imgDirectoryLEdit->text();
    portName = msettings->ui->portNameLEdit->text();
    userName = msettings->ui->userNameLEdit->text() + msettings->ui->experimentLEdit->text();
#ifdef SAVE_MEASURES_LOG_FILE
    QString dateTimeString = "./Logs/measures/1" +userName+ QDateTime::currentDateTime().toString() +".log";
    measurementLog.setFileName(dateTimeString);
#endif
    if (!msettings->ui->missionMapGb->isChecked()){
        terrain->fill(Qt::black);
    }else{
        if (msettings->ui->imgMapLEdit->text() == ""){

            terrain->fill(Qt::black);
        }else{
            *terrain=QPixmap::fromImage(QImage(msettings->ui->imgMapLEdit->text())).scaled(MAP_WIDTH,MAP_HEIGHT);
        }
        if(isMissionLoaded){
            paintFirstTime=true;
        }
    }
    this->setVisible(true);
}

void MainWindow::on_loadMissionBtn_clicked()
{
    QList<wayPoint_t> wpList;

    QString fileXml = QFileDialog::getOpenFileName(this,
                                                   tr("Mission File"),
                                                   "./Missions",
                                                   tr("XML files (*.xml)"));
    QDomDocument doc( "Talvikki" );
    QFile file(fileXml);
    if( !file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"no lee, aqui va un mensaje y un log";
        return;
    }
    if( !doc.setContent(&file))
    {
      qDebug()<<"no pone el contenido, aqui va un mensaje y un log";
      return;
    }
    file.close();

    QDomElement root = doc.documentElement();
    if( root.tagName() != "Mission" )
    {
        qDebug()<<"Xml no es correcto, aqui va un mensaje y un log";
        return;
    }


    for(int i = 0; i<root.childNodes().size();i++)
    {
//        qDebug()<<root.childNodes().at(i).nodeName();
        QDomNode n = root.childNodes().at(i);
        if(n.nodeName()!="#comment")
        {
          QDomElement e = n.toElement();
          if( !e.isNull() )
          {
//            qDebug()<<"e: "<<e.tagName();
            if( e.tagName() == "WayPoints" )
            {
                for(int j=0;j<e.childNodes().size();j++)
                {
                    QDomElement a = e.childNodes().at(j).toElement();
                    if(a.tagName()=="wp")
                    {
                        wayPoint_t wp;
                            wp.ID = QVariant(a.attribute("ID","")).toInt();
                            wp.altitude = QVariant(a.attribute("alt","")).toFloat();
                            wp.coordinate.lat = QVariant(a.attribute("lat","")).toFloat();
                            wp.coordinate.lon = QVariant(a.attribute("lon","")).toFloat();
                            calculateUTM(&wp.coordinate);
                            wp.yaw = QVariant(a.attribute("yaw","")).toFloat();
                        wpList.insert(wp.ID,wp);
                    }
                }
            }
            if( e.tagName() == "Path" )
            {
                for(int j=0;j<e.childNodes().size();j++)
                {
                    QDomElement a = e.childNodes().at(j).toElement();
                    if(a.tagName()=="step")
                    {
                        mission_t path={0,0,0,0,{0,0,{0,0,0,0},0}};
                            path.wpID = QVariant(a.attribute("wpID","")).toInt();
                            path.photo = QVariant(a.attribute("photo","")).toInt();
                            path.timeDelay = QVariant(a.attribute("timeDelay","")).toFloat();
                            path.err = QVariant(a.attribute("err","")).toFloat();
                            path.wayPoint = wpList.at(path.wpID);
                        missionList.insert(j,path);


                    }
                    else if(a.tagName()=="home")
                    {
                        mission_t path={0,0,0,0,{0,0,{0,0,0,0},0}};
                            path.wpID = -1;
                            path.photo = QVariant(a.attribute("photo","")).toInt();
                            path.timeDelay = QVariant(a.attribute("timeDelay","")).toFloat();
                            path.err = QVariant(a.attribute("err","")).toFloat();
                            path.wayPoint.altitude = QVariant(a.attribute("alt","")).toFloat();
                            path.wayPoint.yaw=QVariant(a.attribute("yaw","")).toFloat();
                            path.wayPoint.coordinate = homeCoord;
                        missionList.insert(j,path);
                    }
                }
            }
            if( e.tagName() == "Config" ){
                QDomElement a = e.toElement();
                confg.yawErr = QVariant(a.attribute("yaw_err","")).toFloat();
                confg.altErr = QVariant(a.attribute("alt_err","")).toFloat();
                confg.distWp = QVariant(a.attribute("dist_wp","")).toFloat();
                confg.missionTout = QVariant(a.attribute("timeout","")).toFloat();
                confg.boundMapIni.lat=QVariant(a.attribute("max_lat_map","")).toDouble();
                confg.boundMapIni.lon=QVariant(a.attribute("max_lon_map","")).toDouble();
                confg.boundMapEnd.lat=QVariant(a.attribute("min_lat_map","")).toDouble();
                confg.boundMapEnd.lon=QVariant(a.attribute("min_lon_map","")).toDouble();
                calculateUTM(&confg.boundMapEnd);
                calculateUTM(&confg.boundMapIni);
            }
        }
          n = n.nextSibling();
        }
    }
    Q_EMIT mission(&missionList, &confg);
    showMission();
    isMissionLoaded=true;
    paintFirstTime=true;
    ui->clearMissionBtn->setEnabled(true);
    ui->loadMissionBtn->setEnabled(false);
    ui->mapView->update();
}

void MainWindow::on_startMissionBtn_clicked()
{
//    mMissionCtrl->startThread();
}

void MainWindow::on_clearMissionBtn_clicked()
{
    ui->listWidget->clear();
    missionList.clear();
    sceneMap->clear();
    ui->mapView->setBackgroundBrush(*new QBrush());
    ui->mapView->update();
    terrain->fill(Qt::black);
    isMissionLoaded=false;
    ui->clearMissionBtn->setEnabled(false);
    ui->loadMissionBtn->setEnabled(true);

}

void MainWindow::on_FTrimBtn_clicked()
{
    QTimer::singleShot(1,mAtCommand,SLOT(flatTrim()));
}

void MainWindow::on_pushButton_clicked()
{
    tcpSocketCmd->connectToHost(WIFI_MYKONOS_IP,CONTROL_PORT);
    if(tcpSocketCmd->waitForConnected(3000))
    {
        qDebug()<<"conectado!!";
    }
    if(tcpSocketCmd->isOpen()){
        qDebug()<<"Port opened";
    }

    mAtCommand->flag_config=true;
}

void MainWindow::calculateUTM(coordinate_t *coordinate){
    int huso = coordinate->lon/6 +31;
    double lamda = huso*6-183;
    double delta = RAD(coordinate->lon-lamda);
    double a = cos(RAD(coordinate->lat))*sin(delta);
    double epsilon=0.5*log((1+a)/(1-a));
    double nao=atan(tan(RAD(coordinate->lat))/cos(delta))-RAD(coordinate->lat);
    double cos2=cos(RAD(coordinate->lat))*cos(RAD(coordinate->lat));
    double v=(RP/(pow(1+EX_2*cos2,0.5)))*0.9996;
    double rho=EX_2/2*epsilon*epsilon*cos2;
    double A1=sin(2*RAD(coordinate->lat));
    double A2=A1*cos2;
    double J2=RAD(coordinate->lat)+A1/2;
    double J4=(3*J2+A2)/4;
    double J6=(5*J4+A2*cos2)/3;
    double alfa=3*EX_2/4;
    double beta=5*alfa*alfa/3;
    double gamma=35*alfa*alfa*alfa/27;
    double Bo=0.9996*RP*(RAD(coordinate->lat)-alfa*J2+beta*J4-gamma*J6);
    double X= epsilon*v*(1+rho/3)+500000;
    double Y= nao*v*(1+rho)+Bo;
    coordinate->x=X;
    coordinate->y=Y;
//qDebug()<<QString::number( X, 'f', 6 )<<""<< QString::number( Y, 'f', 6 );
}

void MainWindow::showMission()
{
    QString msg="WpID:\tAltitude:\tPsi:\tLatitude:\tLongitude:\tPhoto:\tDelay:\tError:";
    ui->listWidget->addItem(msg); // this header will be at index 0
    // solo para imprimir en la tabla
    for (int i=0;i<missionList.size();i++)
    {
        mission_t tmp=missionList.at(i);
        QString wpID = QString::number(tmp.wpID);
        if(tmp.wpID == -1) wpID='H';
        QString msg1= wpID + "\t" +
                      QString::number(tmp.wayPoint.altitude) + "\t" +
                      QString::number(tmp.wayPoint.yaw) + "\t" +
                      QString::number(tmp.wayPoint.coordinate.lat,'f',6) + "\t" +
                      QString::number(tmp.wayPoint.coordinate.lon,'f',6) + "\t" +
                      QString::number(tmp.photo) + "\t" +
                      QString::number(tmp.timeDelay) + "\t" +
                      QString::number(tmp.err);
        ui->listWidget->addItem(msg1);
    }


}

void MainWindow::on_startMissionBtn_2_clicked()
{
    on_startMissionBtn_clicked();
}
void MainWindow::testCoordinates(QString lati,QString latiO,QString longi,QString longiO){

    double lat =lati.mid(0,2).toDouble()+
                lati.mid(2).toDouble()/60;
    if(latiO=="S") lat*=-1;
    double lon =longi.mid(0,3).toDouble()+
                longi.mid(3).toDouble()/60;
    if(longiO=="W") lon*=-1;
    coordinate_t coord;
    coord.lat=lat;
    coord.lon=lon;
    calculateUTM(&coord);
    qDebug()<<QString::number(coord.lat, 'f', 6 )<<QString::number(coord.lon, 'f', 6 )
           <<QString::number(coord.x, 'f', 6 )<<QString::number(coord.y, 'f', 6 );
//    update();
 }
void MainWindow::paintEvent(QPaintEvent *)
{
    if(!(terrain->isNull()))
    {
        QPainter painter;
        QPainterPath missionPath,
                     missionPoints,
                     tray,
                     WPtext;
        if(paintFirstTime || isHomesetted)
        {
            painter.begin(terrain);
//            painter.setClipRect(terrain->rect());

            //    float a =(homeCoord.x-confg.boundMapEnd.x)*640/abs(confg.boundMapEnd.x-confg.boundMapIni.x);//24.849662//24.703446, 24.995816
            //    float b = (homeCoord.y-confg.boundMapIni.y)*480/abs(confg.boundMapIni.y-confg.boundMapEnd.y);//18.758993;
            //    missionPoints.moveTo(a,b);
            //    missionPoints.addEllipse(a,b,1,1);
                QFont font("Courier", 10);
            //    WPtext.addText(a,b,font,"H");
            //    missionPath.addEllipse(a-15,b-15,30,30);
                for(int i=0;i<missionList.size();i++)
                {
                   float x = (confg.boundMapEnd.lon - missionList.at(i).wayPoint.coordinate.lon)*MAP_WIDTH/(confg.boundMapEnd.lon-confg.boundMapIni.lon);
                   float y = (confg.boundMapIni.lat - missionList.at(i).wayPoint.coordinate.lat)*MAP_HEIGHT/qAbs(confg.boundMapEnd.lat-confg.boundMapIni.lat);
//                   qDebug()<<QString::number(x,'f',6)<<y;
                   missionPoints.lineTo(x,y);
                   missionPoints.addEllipse(x,y,1,1);
                   WPtext.addText(x,y,font,QString::number(i));
            //           painter.drawText(x,y,10,12,Qt::AlignBottom,QString::number(i));
                   missionPath.addEllipse(x-15,y-15,30,30);
                }
            painter.setPen(Qt::yellow);
            QBrush brushY (Qt::yellow,Qt::SolidPattern);
            painter.setBrush(brushY);
            painter.setOpacity(0.3);
            painter.drawPath( missionPath );
            painter.setOpacity(1);
            painter.setPen(Qt::red);
            painter.drawPath(missionPoints);
            painter.setPen(Qt::blue);
            painter.drawPath(WPtext);
            painter.end();

            ui->mapView->setBackgroundBrush(*terrain);
            ui->mapView->setCacheMode(QGraphicsView::CacheBackground);
//            sceneMap->addPixmap(*terrain);
            ui->mapView->update();
            paintFirstTime=false;
            isHomesetted=false;
        }
        if(newGpsData)
        {
//            QList<QGraphicsItem* > viewItems =  sceneMap->items(terrain->rect());
//           qDebug()<<viewItems;
//           if(sceneMap->items(terrain->rect()).size()>10)
//           {
//               sceneMap->removeItem(sceneMap->items(terrain->rect()).takeLast());
//           }
            sceneMap->clear();
            QPixmap *terrain1 = new QPixmap(640,480);
            terrain1->fill(Qt::transparent);
//            QPainter painter1(terrain1);
            painter.begin(terrain1);
            float a = (confg.boundMapIni.lon - currCoord.lon)*MAP_WIDTH/qAbs(confg.boundMapEnd.lon-confg.boundMapIni.lon);
            float b = (confg.boundMapIni.lat - currCoord.lat)*MAP_HEIGHT/qAbs(confg.boundMapEnd.lat-confg.boundMapIni.lat);
//            qDebug()<<a<<b<<currCoord.lon<<currCoord.lat<<(currCoord.lon-confg.boundMapIni.lon);
//          tray.lineTo(a,b);
//          tray.addRoundRect(a,b,3,3,1);
            painter.setPen(Qt::green);
            QBrush brushG (Qt::green,Qt::SolidPattern);
            painter.setBrush(brushG);
            painter.drawEllipse(a,b,3,3);
//            painter.drawPath(tray);
            painter.end();
            sceneMap->addPixmap(*terrain1);
            ui->mapView->update();
            newGpsData=false;
        }

    }
}
void MainWindow::showCurrentStep(int i)
{
    ui->listWidget->setCurrentRow(i+1);
    ui->currStepValue->setText(QString::number(i+1));
    ui->timeoutValue->setText(QString::number(confg.missionTout) + " s");
    ui->stepLatValue->setText(QString::number(missionList.at(i).wayPoint.coordinate.lat, 'f', 6));
    ui->stepLonValue->setText(QString::number(missionList.at(i).wayPoint.coordinate.lon, 'f', 6));
    ui->stepXValue->setText(QString::number(missionList.at(i).wayPoint.coordinate.x, 'f', 6));
    ui->stepYValue->setText(QString::number(missionList.at(i).wayPoint.coordinate.y, 'f', 6));
    ui->stepYawValue->setText(QString::number(missionList.at(i).wayPoint.yaw) + QChar(0x00BA));
    ui->stepAltValue->setText(QString::number(missionList.at(i).wayPoint.altitude) + " mm");
    ui->stepDelayValue->setText(QString::number(missionList.at(i).timeDelay) + " s");
    QString photo="";
    if(missionList.at(i).photo==1)
        photo="horizontal Cam";
    else if(missionList.at(i).photo==2)
        photo="Vertical Cam";
    else
        photo="No photo";
    ui->stepPhotoValue->setText(photo);
    ui->stepGPSErrValue->setText(QString::number(missionList.at(i).err) + " m");
    ui->stepYawErrValue->setText(QString::number(confg.yawErr) + QChar(0x00BA));
    ui->stepAltErrValue->setText(QString::number(confg.altErr) + " mm");
}
void MainWindow::on_chageCamBtn_clicked()
{
    QTimer::singleShot(1,mAtCommand,SLOT(changeCamera()));
}

void MainWindow::on_imgMapBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load the Mission Map"),
                                                    "./maps",
                                                    tr("Image Files (*.png *.jpg *.bmp)"));
    msettings->ui->imgMapLEdit->setText(fileName);
}
void MainWindow::newConfDataReady()
{
//    qDebug() << "Reading: "<< tcpSocketCmd->bytesAvailable();
    qint64 size = tcpSocketCmd->bytesAvailable();
    QByteArray datagram;
    datagram.resize(size);
    tcpSocketCmd->read(datagram.data(), size);
    QList<QByteArray> lines = datagram.split('\n');
    ui->tableWidget->setRowCount(lines.size()-1);
    for(int i=0; i<lines.size()-1; i++){
        QList<QByteArray> fields = lines.at(i).split('=');
        QString *a = new QString(fields.at(0).data());
        QString *b = new QString(fields.at(1).data());
        ui->tableWidget->setItem(i,0,new QTableWidgetItem(*a,0));
        ui->tableWidget->setItem(i,1,new QTableWidgetItem(*b,1));
    }

//    qDebug() << tcpSocketCmd->readAll();
    tcpSocketCmd->close();
}

void MainWindow::on_captureFramesBtn_clicked()
{
    if(recFrames){
        recFrames=false;
        ui->captureFramesBtn->setText("Rec Frames");
    }else{
        recFrames=true;
        ui->captureFramesBtn->setText("Stop!");
    }

}

void MainWindow::on_followPatternBtn_clicked()
{
    int cntrl=0;
    if(ui->CocardeCheckBox->isChecked()) cntrl=1;
    Q_EMIT detectTag(cntrl);
//    QTimer::singleShot(1,mAtCommand,SLOT(detectTag()));
//    mMissionCtrl->startThread();
}

void MainWindow::on_emgResetBtn_clicked()
{
    QTimer::singleShot(1,mAtCommand,SLOT(emergencyPressed()));
}
