/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include <ardrone_control_station/qnode.h>
#include <qdebug.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace prueba_qt {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :
	init_argc(argc),
	init_argv(argv)
	{}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
	wait();
}

bool QNode::init() {
	ros::init(init_argc,init_argv,"prueba_qt");
        ros::Time::init();
	if ( ! ros::master::check() ) {
		return false;
	}
        ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
	// Add your ros communications here.
    image_transport::ImageTransport it(n);
    image_pub = it.advertise("/parrot/image", 1);
    telemetry_pub = n.advertise<std_msgs::String>("/parrot/telemetry", 1000);
	start();
	return true;
}

bool QNode::init(const std::string &master_url, const std::string &host_url) {
	std::map<std::string,std::string> remappings;
	remappings["__master"] = master_url;
	remappings["__hostname"] = host_url;
	ros::init(remappings,"prueba_qt");
	if ( ! ros::master::check() ) {
		return false;
	}
	ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
	// Add your ros communications here.
    image_transport::ImageTransport it(n);
    image_pub = it.advertise("/parrot/image", 1);
    start();
    return true;
}

void QNode::run() {
    qDebug()<<"my niga";
	ros::Rate loop_rate(1);
//	int count = 0;
        while ( ros::ok() ) {
            ros::spinOnce();
            loop_rate.sleep();
//
//        sensor_msgs::ImagePtr msg;
//        IplImage* image_;
//        image_ = cvLoadImage("/home/david/Downloads/1.jpg");
//        msg = sensor_msgs::CvBridge::cvToImgMsg(image_, "bgr8");
//        image_pub.publish(msg);
//        log(Info,std::string("I sent an image "));
//        ros::spinOnce();
//        loop_rate.sleep();
//        ++count;
        }
	std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
  Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
}


void QNode::log( const LogLevel &level, const std::string &msg) {
	logging_model.insertRows(logging_model.rowCount(),1);
	std::stringstream logging_model_msg;
	switch ( level ) {
		case(Debug) : {
				ROS_DEBUG_STREAM(msg);
				logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Info) : {
				ROS_INFO_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Warn) : {
				ROS_WARN_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Error) : {
				ROS_ERROR_STREAM(msg);
				logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Fatal) : {
				ROS_FATAL_STREAM(msg);
				logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
				break;
		}
	}
	QVariant new_row(QString(logging_model_msg.str().c_str()));
	logging_model.setData(logging_model.index(logging_model.rowCount()-1),new_row);
  Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}
IplImage* QNode::QImage2IplImage(QImage* qimg)
{

IplImage *imgHeader = cvCreateImageHeader( cvSize(qimg->width(), qimg->height()), IPL_DEPTH_8U, 3);
imgHeader->imageData = (char*) qimg->bits();

uchar* newdata = (uchar*) malloc(sizeof(uchar) * qimg->byteCount());
memcpy(newdata, qimg->bits(), qimg->byteCount());
imgHeader->imageData = (char*) newdata;
//cvClo
return imgHeader;
}
void QNode::rcvImage(QImage Img){

   // sensor_msgs::ImagePtr msg;
    cv_bridge::CvImagePtr cv_ptr;

    IplImage* image_;
  //  image_ = cvLoadImage("/home/mario/Downloads/1.jpg");
  //  qDebug()<<"Image loaded";
   // cv::WImageBuffer3_b image_( cvLoadImage("/home/mario/Downloads/1.jpg", CV_LOAD_IMAGE_COLOR) );
    try
    {
        image_= QImage2IplImage(&Img.convertToFormat(QImage::Format_RGB888));
        cv_ptr->image = image_;


     //   msg = sensor_msgs::CvBridge::cvToImgMsg(image_, "rgb8");
     //     msg = sensor_msgs::CvBridge::cvToImgMsg(image_.Ipl(), "bgr8");
        //  qDebug()<<"[Img publisher]Image Converted";
        image_pub.publish(cv_ptr->toImageMsg());
    }
    catch(cv_bridge::Exception& e){
    qDebug()<<"Error!! "<< e.what();
    ROS_ERROR("cv_bridge exception: %s", e.what());
    }
//    log(Info,std::string("I sent an image "));

}
void QNode::publishNavdata(telemetry_t *data, coordinate_t *data1){
    QString auxMsg = "\n" +
                  QString("latitude ") + QString::number(data1->lat,'f',9) + "\n" +
                  "longitude " + QString::number(data1->lon,'f',9) + "\n" +
                  "utmx " + QString::number(data1->x,'f',9) + "\n" +
                  "utmy " + QString::number(data1->y,'f',9) + "\n" +
                  QString("Roll (phi) ") + QString::number(data->tmpPhi,'f',3) + "\n" +
                  QString("Yaw (psi) ") + QString::number(data->tmpPsi,'f',3) + "\n" +
                  QString("Pitch (theta) ") + QString::number(data->tmpTheta,'f',3) + "\n" +
                  "altitude " + QString::number(data->tmpAltitude) + "\n" +
                  "battery " + QString::number(data->tmpBat) + "\n" +
                  "tag detected " + QString::number(data->nb_detected) + "\n" +
                  "tag x " + QString::number(data->xc) + "\n" +
                  "tag y " + QString::number(data->yc) + "\n" +
                  "tag angle " + QString::number(data->ac,'f',3) + "\n" +
                  "tag height " + QString::number(data->tag_height) + "\n" +
                  "tag width " + QString::number(data->tag_width) + "\n" +
                  "motor1 " + QString::number(data->motor1) + "\n" +
                  "motor2 " + QString::number(data->motor2) + "\n" +
                  "motor3 " + QString::number(data->motor3) + "\n" +
                  "motor4 " + QString::number(data->motor4) + "\n" +
                  QString("current motor1 ") + QString::number(data->current_motor1) + "\n" +
                  QString("current motor2 ") + QString::number(data->current_motor2) + "\n" +
                  QString("current motor3 ") + QString::number(data->current_motor3) + "\n" +
                  QString("current motor4 ") + QString::number(data->current_motor4) + "\n" +
                  QString("State ") + QString::number(data->state);
    std_msgs::String msg;
//    std::stringstream ss;
//    ss << auxMsg.toStdString();
//    msg.data = ss.str();
    msg.data = auxMsg.toStdString();
    telemetry_pub.publish(msg);
//    qDebug()<<msg;



//    ros::NodeHandle::advertise<std_msgs::String>("chatter", 1000);
}





}  // namespace prueba_qt
