#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
    class Settings;
}

class Settings : public QDialog {
    Q_OBJECT
public:
    Settings(QWidget *parent = 0);
    Ui::Settings *ui;
    ~Settings();

protected:
    void changeEvent(QEvent *e);

//private:

};

#endif // SETTINGS_H
