/*

	threadcomport v.0.0.1
	author Golubkin Egor (Gorin), Russia
	///////////////////////////////////////////////

	Multiplatform asynchronous Serial Port Extension
	based on Wayne Roth's QExtSerialPort
	

*/  
#include <ardrone_control_station/threadcomport.h>


/*
==============
<CONSTRUCTORS>
==============
*/
Qthreadcomport::Qthreadcomport (const PortSettings *settings):
        QextSerialPort(*settings)
{
    thread = NULL;
    ftimeout = 0;
}

ReceiveThread::ReceiveThread()
{
    lastcount = 0;
    ftimeout = 0;
}

QMainComThread::QMainComThread(QString name)
{
    comsettings = new PortSettings();

    comsettings->BaudRate=BAUD115200;
    comsettings->FlowControl=FLOW_OFF;
    comsettings->Parity=PAR_NONE;
    comsettings->DataBits=DATA_8;
    comsettings->StopBits=STOP_1;
    comname = name;
    comport = NULL;
}
/*
==============
<DESTRSTRUCTORS>
==============
*/
Qthreadcomport::~Qthreadcomport()
{
    stopThread();
}
QMainComThread::~QMainComThread()
{
    terminate();
    wait();
    delete comport;
    comport = NULL;
    delete comsettings;
    comsettings = NULL;
}
/*
==============
<METHODS>
==============
*/
/*TMainComThread*/
Qthreadcomport* QMainComThread::getPort()
{
    return comport;
}

void QMainComThread::run()
{
//    qDebug() << "[comPort] run: "<<(pid_t) syscall (SYS_gettid);
    Qthreadcomport *port = new Qthreadcomport(comsettings);
    port->setPortName(comname);
    comport = port;

    exec();
}

/*QReceiveThread*/
void ReceiveThread::run()
{
//     qDebug() << "[recievePort] run: "<<(pid_t) syscall (SYS_gettid);
    int count;
    QTime timerTimeout;
    timerTimeout.start();
    Q_FOREVER
    {
        msleep(100);// este valor se puede variar ya que cada lectura llega cada 0.25 segundos
        mutex.lock();
        count = comport->bytesAvailable();
        mutex.unlock();
//        emit newDataInPo7rtThread(count);
        if (count >= 74)
        {

            if (timerTimeout.elapsed() > ftimeout)
                {
                    Q_EMIT newDataInPortThread(count);
//                    QTime timedb;
//                    qDebug()<<"thread count= "<<count<<"time= "<<"\t"<<timedb.currentTime().second()<<" "<<timedb.currentTime().msec();
                    timerTimeout.restart();
                }
        }
        if (timerTimeout.elapsed() > ftimeout)
            timerTimeout.restart();
    }
}

void ReceiveThread::setPort(Qthreadcomport *port)
{
    comport = port;
}

/*Qthreadcomport*/
void Qthreadcomport::close()
{
    stopThread();
    QextSerialPort::close();
}

bool Qthreadcomport::open(QIODevice::OpenMode mode)
{
    thread = new ReceiveThread();
    thread->setPort(this);
    connect(thread,SIGNAL(newDataInPortThread(int)),this,SLOT(newDataInPortSlot(int)));
    bool bopen = QextSerialPort::open(mode);
    thread->start();
    thread->setTimeout(ftimeout);
    return bopen;
}

qint64 Qthreadcomport::readData(char *data, qint64 maxSize)
{
    qint64 count = QextSerialPort::readData(data, maxSize);
    return count;
}

void Qthreadcomport::stopThread()
{
    if (thread != NULL)
    {
        thread->terminate();
        thread->wait();
        delete thread;
        thread = NULL;
    }
}

void Qthreadcomport::setTimeout(int timeout)
{
    if (thread != NULL)
        thread->setTimeout(timeout);
    ftimeout = timeout;
}

/*
==============
<SLOTS>
==============
*/
void Qthreadcomport::newDataInPortSlot(int count)
{
    if ((bytesAvailable() == 0) || (!(isOpen()))) return;
    const int size = 4096;
    char data[size];
    int readcount = readData(data, count);
    QTime timedb;
    timedb = timedb.currentTime();
//    qDebug()<<"readData"<<"time= "<<"\t\t\t"<<timedb.second()<<" "<<timedb.msec();
//    qDebug()<<data;
//    data[readcount] = '\0';
    unsigned char *dataPtr;
    dataPtr = (unsigned char *)&data;
    /**************************************/
    QHash<int,QString> *msg;
    msg= new QHash<int,QString>();
//    QHash<int,QByteArray> msg;
    coordinate_t currCoord;
    int c=1;
    QString tmp;
//    QByteArray tmp;
    uint chksum= 0;
    int i = 0;
    if('\x24'==dataPtr[i]){

        bool chkFlag=true;
        for (;i < readcount;i++)
        {
            switch (dataPtr[i])
            {
                case '\x2C': //,
                    msg->insert(c,tmp);
                    c++;
                    tmp.clear();
                    break;
                case '\x2A': //*
                    msg->insert(c,tmp);
                    tmp.clear();
                    tmp.append(data[i+1]);
                    tmp.append(data[i+2]);
                    msg->insert(CHECKSUM,tmp);
                    tmp.clear();
                    c=1;
                    chkFlag=false;
                    break;
                default:
                    tmp += data[i];
            }
            // iterate over the string, XOR each byte with the total sum:
            if ('\x24'!=dataPtr[i]&&chkFlag) {
                chksum ^= dataPtr[i];
            }
        }
    }
    QByteArray checksum;
    checksum.append(chksum);
//    qDebug()<<msg->value(ID_MSG)
//            <<msg->value(UTC)
//            <<msg->value(STATUS)
//            <<msg->value(LATITUDE)
//            <<msg->value(LAT_ORIENTATION)
//            <<msg->value(LONGITUDE)
//            <<msg->value(LON_ORIENTATION)
//            <<msg->value(MAG_VAR)
//            <<msg->value(M_V_ORIENTATION)
//            <<msg->value(SPEED)
//            <<msg->value(COURSE)
//            <<msg->value(DATE)
//            <<msg->value(MODE)
//            <<msg->value(CHECKSUM);
//    qDebug()<<checksum.toHex()<<msg->value(CHECKSUM);

    if(0==QString::compare(msg->value(CHECKSUM),checksum.toHex(),Qt::CaseInsensitive)){

        if(msg->value(STATUS)=="A")
        {
    //        testCoordinates(msg.value(LATITUDE), msg.value(LAT_ORIENTATION),msg.value(LONGITUDE), msg.value(LON_ORIENTATION));
            double lat =msg->value(LATITUDE).mid(0,2).toDouble()+
                        msg->value(LATITUDE).mid(2).toDouble()/60;
            if(msg->value(LAT_ORIENTATION)=="S") lat*=-1;

            double lon =msg->value(LONGITUDE).mid(0,3).toDouble()+
                        msg->value(LONGITUDE).mid(3).toDouble()/60;
            if(msg->value(LON_ORIENTATION)=="W") lon*=-1;

            currCoord.lat=lat;
            currCoord.lon=lon;
            Q_EMIT newDataInPortSignal(&currCoord);
        }else
        {
            QString errMsg= "GPS data not valid";
            qDebug()<<errMsg;
    //        ui->timeUTCValue->setText("<font color='red'>"+ errMsg +"</font>");
    //        ui->longitudeValue->setText("<font color='red'>"+ errMsg +"</font>");
    //        ui->laltitudeValue->setText("<font color='red'>"+ errMsg +"</font>");
    //        ui->xUTMValue->setText("<font color='red'>"+ errMsg +"</font>");
    //        ui->yUTMValue->setText("<font color='red'>"+ errMsg +"</font>");
        }

    }else{qDebug()<<"Bad Checksum"<<msg->value(CHECKSUM)<<checksum.toHex();}
      /*************************************/

}

