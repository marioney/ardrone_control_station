#ifndef MISSIONCTRL_H
#define MISSIONCTRL_H
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <ardrone_control_station/mainwindow.h>
#include <ardrone_control_station/navdatahandler.h>
#include <ardrone_control_station/atcommand.h>

#include <ardrone_control_station/Joystick.h>
#include <QHostAddress>
#include <QUdpSocket>
#include <QByteArray>
#include <QTimer>
#include <QDataStream>
#include <QTcpSocket>
#include <sys/syscall.h>
class missionCtrl : public QThread
{
    Q_OBJECT

public:
    missionCtrl(AtCommand *mAtCmd, QObject *parent = 0 );
    ~missionCtrl();

    void startThread();

private:
    qreal calculateVector();

protected:
    void run();

private Q_SLOTS:
    void getTelemetry(telemetry_t *);
    void getMission(QList<mission_t> * ,missionConf_t *);
    void getCurrLocation( coordinate_t *);
    void abortMission(int);
    void camVert(bool);
    void handletout();


Q_SIGNALS:
    void sendData(qreal,qreal);
    void sendAxis(qreal,qreal);
    void changeCamera();
    void emergencyPressed();
    void startPressed();
    void stopPressed();
    void saveImg();
    void showCurrentStep(int);

private:
    AtCommand *mAtCommand;
    QMutex mMutex;
    QWaitCondition mCondition;
    bool mAbort, abortMssn, toutFlag;
    telemetry_t currTelemetry;
    coordinate_t currCoord;
    missionConf_t config;
    QList<mission_t> missionList;
    mission_t paso_actual;
    double mag;
    bool cam;

};

#endif // MISSIONCTRL
