#include <ardrone_control_station/navdatahandler.h>

/*!
    Constructs the NavDataHandler.
    The \a parent parameter is sent to the QThread constructor.
*/
NavDataHandler::NavDataHandler(QObject *parent) : QThread(parent)
{
    qDebug() << "[NavDataHandler] Constructor";

//    qRegisterMetaType<telemetry_t>("telemetry_t");
    mAbort = false;
    initialize();
}

/*!
    Destructor.
    Waits for thread to abort before destroying the \c NavDataHandler object.
*/
NavDataHandler::~NavDataHandler()
{
    qDebug() << "[NavDataHandler] Destructor";
    mMutex.lock();
    mAbort = true;
    mCondition.wakeOne();
    mMutex.unlock();
    wait();
}

/*!
    This function sets up a UDP socket and sends a message to the drone that we are ready to
    receive data.
*/
void NavDataHandler::initialize()
{
    qDebug() << "[NavDataHandler] Initialize";

    QMutexLocker locker(&mMutex);

    mDroneResponded = false;

    // UDP socket for receiving data from the drone.
    mUdpSocketNavData = new QUdpSocket(this);
    bool res = mUdpSocketNavData->bind(NAVDATA_PORT, QUdpSocket::ShareAddress);
    if (!res) {
        qDebug() << "[NavDataHandler] Error connecting to Navdata " << " result:" << res;
    } else {
        connect(mUdpSocketNavData, SIGNAL(readyRead()),this, SLOT(newNavDataReady()));
    }

    // Send message to drone to signal that we are ready to receive data.
    QHostAddress reciever;
    int res2;
    const char data[] = "\1\0\0\0";
    reciever = QHostAddress::QHostAddress(WIFI_MYKONOS_IP);
    res2 = mUdpSocketNavData->writeDatagram(data,reciever,NAVDATA_PORT);
    if (res2 == -1) {
        qDebug() << "[NavDataHandler] Error initializing NavData UPD socket.";
    }

    mInitTimer = new QTimer(this);
    connect(mInitTimer, SIGNAL(timeout()), this, SLOT(initTimer()));
    mInitTimer->start(200);
}

/*!
    Thread main function.
    Reads status data received from the drone.
*/
void NavDataHandler::run()
{
//    qDebug() << "[NavDataHandler] run: "<<(pid_t) syscall (SYS_gettid);

    uint sequence = NAVDATA_SEQUENCE_DEFAULT-1;
    uint droneState = 0;
    bool emergencyState = false;
    telemetry_t telemetry;

    Q_FOREVER {
        // Do the stuff but check if we need to abort first...
        if (mAbort) {
            return;
        }

        QByteArray datagram;

        // Copy the next datagram in the list to a variable that is local to the thread.
        mMutex.lock();
        if (!mDatagramList.empty()) {
            datagram = mDatagramList.takeFirst();
        }
        mMutex.unlock();

        int size = datagram.size();

        if (size == 0) {
            sequence = NAVDATA_SEQUENCE_DEFAULT-1;
        } else {
            navdata_t* navdata = (navdata_t*)datagram.data();
            if (navdata->header == NAVDATA_HEADER) {
                if (get_mask_from_state(navdata->ardrone_state, ARDRONE_NAVDATA_BOOTSTRAP)) {
                    qDebug() << "[NavDataHandler] ARDRONE_NAVDATA_BOOTSTRAP";
                    mDroneBooting = true;
                    Q_EMIT sendInitSequence(1);
                } else if (mDroneBooting && get_mask_from_state(navdata->ardrone_state, ARDRONE_COMMAND_MASK)) {
                    qDebug() << "[NavDataHandler] ARDRONE_COMMAND_MASK";
                    mDroneBooting = false;
                    Q_EMIT sendInitSequence(2);
                } else {
                    // If drone status has changed, signal it to the other components.
                    if (droneState != navdata->ardrone_state) {
                        droneState = navdata->ardrone_state;
    //                    qDebug()<< "dronestate:  "<<droneState;
                        Q_EMIT updateARDroneState(droneState);
                    }

                    // If emergency state has changed, signal it to the other components.
                    if (emergencyState != get_mask_from_state(droneState, ARDRONE_EMERGENCY_MASK)) {
                        emergencyState = get_mask_from_state(droneState, ARDRONE_EMERGENCY_MASK);
                        Q_EMIT updateEmergencyState(emergencyState);
                    }

                    // If the drone communication watchdog has been active then the sequence numbering
                    // has been restarted.
                    if (get_mask_from_state(droneState, ARDRONE_COM_WATCHDOG_MASK)) {
                        sequence = NAVDATA_SEQUENCE_DEFAULT-1;
                    }

                    // Check if this is not old data we have received.
                    if ( navdata->sequence > sequence ) {
                        quint32 navdata_cks = 0;
                        navdata_unpacked_5_t navdata_unpacked;
                        bool ret = ardrone_navdata_unpack_all_5(&navdata_unpacked, navdata, &navdata_cks);
                        if (ret) {
                            // Compute checksum.
                            quint32 cks = navdata_compute_cks((quint8*)datagram.data(), size-sizeof(navdata_cks_t));
                            // Compare checksums to see if data is valid.
                            if (cks == navdata_cks) {
                                // Data is valid.
                                telemetry.tmpBat = navdata_unpacked.navdata_demo.vbat_flying_percentage;
                                telemetry.tmpAltitude = navdata_unpacked.navdata_demo.altitude;
                                telemetry.tmpPhi = navdata_unpacked.navdata_demo.phi;
                                telemetry.tmpPsi = navdata_unpacked.navdata_demo.psi;
                                telemetry.tmpTheta = navdata_unpacked.navdata_demo.theta;
//                                telemetry.vx = navdata_unpacked.navdata_demo.vx;
//                                telemetry.vy = navdata_unpacked.navdata_demo.vy;
//                                telemetry.vz = navdata_unpacked.navdata_demo.vz;
                                telemetry.vx = navdata_unpacked.navdata_vision_raw.vision_tx_raw;
                                telemetry.vy = navdata_unpacked.navdata_vision_raw.vision_ty_raw;
                                telemetry.vz = navdata_unpacked.navdata_vision_raw.vision_tz_raw;
                                telemetry.state = navdata->ardrone_state;

                                telemetry.xc = navdata_unpacked.navdata_vision_detect.xc[0];
                                telemetry.yc = navdata_unpacked.navdata_vision_detect.yc[0];
                                telemetry.ac = navdata_unpacked.navdata_vision_detect.orientation_angle[0];
                                telemetry.tag_width = navdata_unpacked.navdata_vision_detect.width[0];
                                telemetry.tag_height = navdata_unpacked.navdata_vision_detect.height[0];
                                telemetry.tag_dist = navdata_unpacked.navdata_vision_detect.dist[0];
                                telemetry.nb_detected = navdata_unpacked.navdata_vision_detect.nb_detected;
                                telemetry.motor1 = navdata_unpacked.navdata_pwm.motor1;
                                telemetry.current_motor1 = navdata_unpacked.navdata_pwm.current_motor1;
                                telemetry.motor2 = navdata_unpacked.navdata_pwm.motor2;
                                telemetry.current_motor2 = navdata_unpacked.navdata_pwm.current_motor2;
                                telemetry.motor3 = navdata_unpacked.navdata_pwm.motor3;
                                telemetry.current_motor3 = navdata_unpacked.navdata_pwm.current_motor3;
                                telemetry.motor4 = navdata_unpacked.navdata_pwm.motor4;
                                telemetry.current_motor4 = navdata_unpacked.navdata_pwm.current_motor4;

                                Q_EMIT updateNavdata(&telemetry);

                            } else {
                                qDebug() << "[NavDataHandler] Checksum failed!";
                            }
                            //qDebug() << "[NavDataHandler]" << tmpAltitude <<" \/"<< tmpPhi <<" \/"<< tmpPsi <<" \/"<<tmpTheta;
                        }
                    }
                    sequence = navdata->sequence;
                }
            }
        }

        // If we have more data in queue, then wait 10 milliseconds before continuing.
        // Else wait until new data arrives.
        mMutex.lock();
        if (mDatagramList.count() > 0) {
            mCondition.wait(&mMutex, 10);
        } else {
            mCondition.wait(&mMutex);
        }
        mMutex.unlock();
    }
}

/*!
    Slot called whenever new data is ready from the drone.
    Starts the thread with NormalPriority unless it is already running.
*/
void NavDataHandler::newNavDataReady()
{
    int size = mUdpSocketNavData->pendingDatagramSize();
    QByteArray datagram;
    datagram.resize(size);
    mUdpSocketNavData->readDatagram(datagram.data(), size);
    mMutex.lock();
    mDatagramList.append(datagram);
    mMutex.unlock();
    if (!isRunning()) {
        mDroneResponded = true;
        start(NormalPriority);
    } else {
        mCondition.wakeOne();
    }
}

void NavDataHandler::initTimer()
{
    if (!mDroneResponded) {
        QHostAddress reciever;
        const char data[] = "\1\0\0\0";
        reciever = QHostAddress::QHostAddress(WIFI_MYKONOS_IP);
        int res = mUdpSocketNavData->writeDatagram(data,reciever,NAVDATA_PORT);
        if (res == -1) {
            qDebug() << "[NavDataHandler] Error initializing NavData UPD socket.";
        }
    } else {
        mInitTimer->stop();
    }
}
