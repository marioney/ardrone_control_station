/*

	threadcomport v.0.0.1
	author Golubkin Egor (Gorin), Russia
	///////////////////////////////////////////////

	Multiplatform asynchronous Serial Port Extension
	based on Wayne Roth's QExtSerialPort
	

*/ 

#ifndef THREADCOMPORT_H
#define THREADCOMPORT_H

#include <ardrone_control_station/qextserialport.h>

#include <QThread>
#include <QTime>
#include <QMutex>
//#include <math.h>
#include <qmath.h>
//#include <QtCore>
#include <QtDebug>
//#include <QtCore/QObject>
//#include <QtCore/QByteArray>
//#include <QtCore/QTime>
#include <sys/syscall.h>
#include <ardrone_control_station/Constants.h>






class Qthreadcomport;
class ReceiveThread;
class QMainComThread;

class QMainComThread : public QThread
{
    Q_OBJECT
public:
    QMainComThread(QString name);
    ~QMainComThread();
    Qthreadcomport* getPort();//return * on Qthreadcomport, used for set settings
protected:
    PortSettings *comsettings;
    Qthreadcomport *comport;
    QString comname;
    void run();
};

class Qthreadcomport : public QextSerialPort {
    Q_OBJECT
public:
    Qthreadcomport(const PortSettings *settings = 0);
    ~Qthreadcomport();
    virtual void close();
    virtual bool open(QIODevice::OpenMode mode=QIODevice::ReadWrite);
    virtual qint64 readData(char *data, qint64 maxSize);
    void setTimeout(int timeout);

Q_SIGNALS:
    void newDataInPortSignal(coordinate_t* currCoord);

private:
    ReceiveThread *thread;
    void stopThread();
    int ftimeout;

private Q_SLOTS:
    void newDataInPortSlot(int count);

};

class ReceiveThread : public QThread
{
    Q_OBJECT
public:
    ReceiveThread();
    void setTimeout(int timeout){ftimeout = timeout;};
    void setPort(Qthreadcomport *port);

Q_SIGNALS:
    void newDataInPortThread(int count);

protected:
    void run();

private:
    QMutex mutex;
    Qthreadcomport *comport;
    int lastcount;
    int ftimeout;
};

#endif
