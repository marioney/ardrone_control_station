#include <ardrone_control_station/atcommand.h>
#include <ardrone_control_station/Joystick.h>

/*!
    Constructs the \c AtCommand.
    The \a parent parameter is is sent to the QThread constructor.
*/
AtCommand::AtCommand(QObject *parent) : QThread(parent)
{
    qDebug() << "[AtCommand] Constructor";
    mAbort = false;
    mARDroneState = 0;
    mPhi = 0;
    mTheta = 0;
    mGaz = 0;
    mYaw = 0;
    mUserInput = (uint)ARDRONE_NO_TRIM;
    InitnbSeq=1;
    mCameraChange = false;
    fTrim=false;
    detectT=false;
    flag_config=false;
    OutShell="FALSE";
    OutFlight="TRUE";
    controlT=0;

#ifdef PRINT_INPUT_PER_SECOND
    mIpsMovCounter = 0;
    mIpsDataCounter = 0;
    mIpsSendCounter = 0;
    mIpsTimer = new QTimer(this);
    connect(mIpsTimer, SIGNAL(timeout()), this, SLOT(handleIPSTimer()));
    mIpsTimer->start(1000);
#endif
#ifdef SAVE_COMMANDS_LOG_FILE
    QString dateTimeString = "./Logs/commands/" + QDateTime::currentDateTime().toString() +".log";
    measurementLog.setFileName(dateTimeString);
#endif
}

/*!
    Destructor.
    Waits for thread to abort before destroying the \c AtCommand object.
*/
AtCommand::~AtCommand()
{
    qDebug() << "[AtCommand] Destructor";

    mMutex.lock();
    mAbort = true;
    mCondition.wakeOne();
    mMutex.unlock();
    wait();
}

/*!
    Starts the thread with \c HighPriority unless it is already running.
*/
void AtCommand::startThread()
{
    qDebug() << "[AtCommand] Starting thread";
    QMutexLocker locker(&mMutex);
    if (!isRunning()) {
        start(HighestPriority);
    } else {
        mCondition.wakeOne();
    }
}

/*!
    Thread main function.
    First it sends initialization messages and then it sends user commands to the drone.
*/
void AtCommand::run()
{
//    qDebug() << "[AtCommand] run: "<<(pid_t) syscall (SYS_gettid);
    // Sequence number used for commands send to drone. The drone will ignore messages with sequence
    // numbers lower than a previously received message, so we need to keep iterating this for each
    // message.
    uint nbSequence = 13; // sequence must start at 13.
    // There a 4 different options for the camera. We will cycle through these 4 when the user
    // presses the camera switch button in InputArea.
    int activeCamera = 0;

    // Hostaddress of the drone.
    QHostAddress hostAddress = QHostAddress::QHostAddress(WIFI_MYKONOS_IP);
    // UDP socket used for sending commands to the drone.
    QUdpSocket *udpSocketAT = new QUdpSocket();
    int res=udpSocketAT->bind(AT_PORT, QUdpSocket::ShareAddress);
    if (-1==res) {
        qDebug() << "[AtCommand] Error connecting to AT port. result: " << res;
        return;
    }
/*
    // This loop is responsible for sending commands to the drone throughout the life of the app.
*/
    Q_FOREVER {
        // Do the stuff but check if we need to abort first...
        if (mAbort) {
            return;
        }

        // Copy values to variables that are local to the sending thread.
        mMutex.lock();
        uint tARDroneState = this->mARDroneState;
        int tPhi = this->mPhi;
        int tTheta = this->mTheta;
        int tGaz = this->mGaz;
        int tYaw = this->mYaw;
        uint tUserInput = this->mUserInput;
        bool tCameraChange = this->mCameraChange;
        mMutex.unlock();

        // Only send commands to the drone if we have received some data from it, meaning that it is
        // up and running.
        if (tARDroneState != 0) {
            QByteArray datagram;
            int cam=0;
            if (get_mask_from_state(tARDroneState, ARDRONE_COM_WATCHDOG_MASK)
                    && !get_mask_from_state(tARDroneState, ARDRONE_NAVDATA_BOOTSTRAP))
            {
                // Reset communication watchdog.
                qDebug() << QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + " [AtCommand] Reset communication watchdog ";
                datagram = "AT*COMWDG=" + NUM_STR(++nbSequence)+"\r";
            }else if(fTrim){
                datagram = "";
                datagram = "AT*FTRIM=" + NUM_STR(++nbSequence)+"\r";
                qDebug()<<"[AtCommand] sending FTRIM "<<datagram;
                fTrim=false;
            }else if(detectT){
                datagram = "";
                //detectar tags
//                datagram = "AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detect_type\",\"2\"\r";
//                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detections_select_h\",\"1\"\r";
//                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detections_select_v_hsync\",\"0\"\r";
//                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:enemy_colors\",\"3\"\r";
                //datectar oriented cocarde
                datagram = "AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detect_type\",\"5\"\r";
                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detections_select_h\",\"0\"\r";
                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"detect:detections_select_v_hsync\",\"4\"\r";
                datagram +="AT*CONFIG=" + NUM_STR(++nbSequence)+",\"control:flying_mode\",\"" + NUM_STR(controlT)+"\"\r";
                qDebug()<<datagram;
                detectT=false;
            }else if(flag_config){
                datagram = "";
                if( tARDroneState & ARDRONE_COMMAND_MASK )
                {
                    datagram = "AT*CTRL=" + NUM_STR(++nbSequence)+",5,0\r";
                }
                datagram+= "AT*CTRL=" + NUM_STR(++nbSequence)+",4,0\r";
                flag_config = false;
            }else{
                datagram = "";

                if (tCameraChange) {
                    // Activate the next of the 4 possible camera options.
                    datagram += "AT*CONFIG=" + NUM_STR(++nbSequence)
                                + ",\"video:video_channel\",\"" + NUM_STR((++activeCamera)%OPT_CAMERA)
                                + "\"\r";
                    mMutex.lock();
                    mCameraChange = false;
                    cam=1;
                    mMutex.unlock();
                }

                if (tPhi != 0 || tTheta != 0 || tGaz != 0 || tYaw != 0) {
                    // The parameters of this command will make the drone move.
                    datagram += "AT*PCMD=" + NUM_STR(++nbSequence)+",1," + NUM_STR(tPhi) + ","
                                + NUM_STR(tTheta) + "," + NUM_STR(tGaz) + "," + NUM_STR(tYaw)+"\r";
//                    qDebug() << QDateTime::currentDateTime().toString("ss.zzz")<<" Sended!";
                } else {
                    // This command will make the drone hoover over the same spot.
                    datagram += "AT*PCMD=" + NUM_STR(++nbSequence)+",0,0,0,0,0\r";
                }
                // The user input send here is Take-off/Land/Emergency/Reset commands.
                datagram += "AT*REF=" + NUM_STR(++nbSequence) + "," + NUM_STR(tUserInput) + "\r";

#ifdef PRINT_INPUT_PER_SECOND
                mMutex.lock();
                mIpsSendCounter++;
                mMutex.unlock();
#endif
#ifdef SAVE_COMMANDS_LOG_FILE
                QTextStream measuresTxt(&measurementLog);
                if(measurementLog.size() <= 0)
                {
                    measurementLog.open(QIODevice::WriteOnly | QIODevice::Text);
                    measuresTxt << "time;nbSequence;tARDroneState;tPhi;tTheta;tGaz;tYaw;tUserInput;tCameraChange\n";
                }else
                {
                    measuresTxt << QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + ";"
                            + QString::number(nbSequence) + ";"
                            + QString::number(tARDroneState) + ";"
                            + QString::number(_phi.f,'f',6) + ";"
                            + QString::number(_theta.f,'f',6) + ";"
                            + QString::number(_gaz.f,'f',6)+";"
                            + QString::number(_yaw.f,'f',6)+";"
                            + QString::number(tUserInput)+";"
                            + QString::number(cam)+";"
                            +"\n";
                }
#endif
                if (get_mask_from_state(tARDroneState, ARDRONE_EMERGENCY_MASK)) {
                    // If the drone is in the emergency state we clear the Emergency and Take-off
                    // bits in the user input, so we are ready for Reset and Take-off again.
                    mMutex.lock();
                    mUserInput &= ~(1 << ARDRONE_UI_BIT_SELECT);
                    mUserInput &= ~(1 << ARDRONE_UI_BIT_START);
                    mMutex.unlock();
                }

            }

                int res=udpSocketAT->writeDatagram(datagram.data(),datagram.size(),hostAddress,AT_PORT);
                if (-1 == res) {
                   qDebug()<<"[AtCommand] Error sending AT data!!"<<datagram;
                }



//            qDebug()<<"[AtCommand] "+ datagram;
        } else {
            qDebug()<<"[AtCommand] No NavData recieved yet";
        }
        // Wait 33 milliseconds before sending the next command.
        mCondition.wait(&mMutex, 33);



    }
}


/*!
    Slot for receiving user input regarding rotation and altitude change, from \c InputArea.

    The parameter \a gaz is altitude change and \a yaw is rotation speed.
*/
void AtCommand::setData(qreal gaz, qreal yaw)
{
#ifdef PRINT_INPUT_PER_SECOND
        mIpsDataCounter++;
#endif
        _gaz.f = gaz;
        _yaw.f = yaw;
        // The received floating point values are saved in integer variables, so they are ready to
        // be sent to the drone.
        mMutex.lock();
        mGaz= _gaz.i;
        mYaw= _yaw.i ;
        mMutex.unlock();
        // Wake thread, since we want to change camera right away.
        if (isRunning()) {
            mCondition.wakeOne();
        }
}

void AtCommand::setAxis(qreal phi, qreal theta)
{
#ifdef PRINT_INPUT_PER_SECOND
        mIpsDataCounter++;
#endif
        _phi.f = phi;
        _theta.f = theta;
        // The received floating point values are saved in integer variables, so they are ready to
        // be sent to the drone.
        mMutex.lock();
        mPhi= _phi.i;
        mTheta= _theta.i ;
        mMutex.unlock();
        // Wake thread, since we want to change camera right away.
        if (isRunning()) {
            mCondition.wakeOne();
        }
//        qDebug() << QDateTime::currentDateTime().toString("ss.zzz")<<" Done!";
}

/*!
    Slot for receiving AR.Drone status change, from \c NavDataHandler.

    The parameter \a state is the AR.Drone status.
*/
void AtCommand::updateARDroneState(uint state)
{
//    qDebug() << "[AtCommand] New state = " << state;
    mMutex.lock();
    mARDroneState = state;
    mMutex.unlock();
    // Wake thread, since we want to react to the new drone state right away.
    if (isRunning()) {
        mCondition.wakeOne();
    } else {
        // This is the first drone state we recieve. Start the thread.
        startThread();
    }
}

/*!
    Slot for receiving camera change commands, from \c InputArea.
*/
void AtCommand::changeCamera()
{
    mMutex.lock();
    mCameraChange = true;
    mMutex.unlock();
    // Wake thread, since we want to change camera right away.
    if (isRunning()) {
        mCondition.wakeOne();
    }
}

/*!
    Slot for receiving indication that the emergency button was pressed in \c InputArea.
*/
void AtCommand::emergencyPressed()
{
    mMutex.lock();
    mUserInput |= (1 << ARDRONE_UI_BIT_SELECT);
    mMutex.unlock();
    // Wake thread, since we want to react to the user pressing Emergency right away.
    if (isRunning()) {
        mCondition.wakeOne();
    }
}

/*!
    Slot for receiving indication that the take/land off button was pressed in \c InputArea.
*/
void AtCommand::startPressed()
{
    mMutex.lock();
        if(get_mask_from_state(mARDroneState, ARDRONE_USER_FEEDBACK_START))
            mUserInput &= ~(1 << ARDRONE_UI_BIT_START);
        else
            mUserInput |= (1 << ARDRONE_UI_BIT_START);
    mMutex.unlock();
}

void AtCommand::flatTrim()
{
    fTrim=true;
}
void AtCommand::detectTag(int control){
    detectT=true;
    controlT=control;
}


void AtCommand::sendInitSequence(int step)
{
    qDebug() << "[AtCommand] sendInitSequence(" << step << ")";
    // Hostaddress of the drone.
    QHostAddress hostAddress = QHostAddress::QHostAddress(WIFI_MYKONOS_IP);

    // UDP socket used for sending commands to the drone.
    QUdpSocket *udpSocketInit = new QUdpSocket();

    // A series of commands are send to the drone in order to make it start up in the correct state.
    QByteArray datagramInit;
    switch (step) {
    case 1:
    {

        // We would like to receive a limited amount of feedback data from the drone. We do not need
        // debug data.
        datagramInit = "AT*CONFIG=1,\"general:navdata_demo\",\"TRUE\"\r";
        // This will make the drone set it's offset values. It is important that the drone is standing
        // on a plain surface during startup!
        int res=udpSocketInit->writeDatagram(datagramInit.data(),datagramInit.size(),hostAddress,AT_PORT);
        if (-1 == res) {
           qDebug()<<"[AtCommand] Error sending navdata_demo config message!";
        }
        qDebug()<<"[AtCommand] "+ datagramInit;


        break;
    }
    case 2:
    {
        wait(100);
        datagramInit = "AT*CTRL=2,5,0\r";
        datagramInit += "AT*CONFIG=3,\"general:navdata_options\",\"" + NUM_STR(NAV_DATA_OPT)+"\"\r";
        // Disable bitrate control mode for the video feed, which is introduced in firmware 1.5.1.
        datagramInit += "AT*CONFIG=4,\"video:bitrate_control_mode\",\"0\"\r";
        // Select the frontcamera as the initial camera to receive video from.
        datagramInit += "AT*CONFIG=5,\"video:video_channel\",\"0\"\r";
        datagramInit += "AT*CONFIG=6,\"control:flight_without_shell\",\""+OutShell+"\"\r";
        datagramInit += "AT*CONFIG=7,\"control:outdoor\",\""+OutFlight+"\"\r";
        datagramInit += "AT*CONFIG=9,\"control:altitude_max\",\"" + NUM_STR(ALT_MAX)+"\"\r";
        datagramInit += "AT*CONFIG=10,\"control:euler_angle_max\",\"" + NUM_STR(PITCH_MAX)+"\"\r";
        datagramInit += "AT*CONFIG=11,\"control:control_vz_max\",\"" + NUM_STR(VZ_MAX)+"\"\r";
        datagramInit += "AT*CONFIG=12,\"control:control_yaw\",\"" + NUM_STR(YAW_MAX)+"\"\r";//150º
        datagramInit += "AT*FTRIM=13\r";
         qDebug()<<"[AtCommand] "+ datagramInit;
        int res=udpSocketInit->writeDatagram(datagramInit.data(),datagramInit.size(),hostAddress,AT_PORT);
        if (!res) {
           qDebug()<<"[AtCommand] Error sending AT config options";
        }

        break;
    }
    default:
        break;
    }
}

#ifdef PRINT_INPUT_PER_SECOND
/*!
    Prints debug messages on the amount of commands received and send.
*/
void AtCommand::handleIPSTimer()
{
    qDebug() << "[AtCommand] Movement/second: " << mIpsMovCounter;
    qDebug() << "[AtCommand] Data/second:     " << mIpsDataCounter;
    mMutex.lock();
    qDebug() << "[AtCommand] Send/second:     " << mIpsSendCounter;
    mIpsSendCounter = 0;
    mMutex.unlock();
    mIpsMovCounter = 0;
    mIpsDataCounter = 0;
}
#endif
