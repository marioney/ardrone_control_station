#ifndef NAVDATAHANDLER_H
#define NAVDATAHANDLER_H
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QUdpSocket>
#include <QtGlobal>
#include <ardrone_control_station/navdata.h>
#include <ardrone_control_station/Constants.h>
#include <QTimer>
#include <sys/syscall.h>
//typedef struct _coodinate_t {
//    double  lat;
//    double  lon;
//    double  x;
//    double  y;
//} coordinate_t;
//typedef struct _telemetry_t
//{
//    uint tmpBat;
//    qint32 tmpAltitude;
//    float tmpPhi;
//    float tmpPsi;
//    float tmpTheta;
//    float vx;
//    float vy;
//    float vz;
//    quint32 xc;
//    quint32 yc;
//    float ac;
//    quint32 nb_detected;
//    quint32   tag_width;
//    quint32   tag_height;
//    quint32   tag_dist;
//    uint state;
//    quint8     motor1;
//    quint16    current_motor1;
//    quint8     motor2;
//    quint16    current_motor2;
//    quint8     motor3;
//    quint16    current_motor3;
//    quint8     motor4;
//    quint16    current_motor4;
//} telemetry_t;

class QTimer;

class NavDataHandler : public QThread
{
    Q_OBJECT
public:
    NavDataHandler(QObject *parent = 0);
    ~NavDataHandler();

protected:
    void run();

Q_SIGNALS:
    void updateARDroneState(uint state);
    void updateEmergencyState(bool on);
    void updateNavdata(telemetry_t *);
    void sendInitSequence(int step);

private Q_SLOTS:
    void newNavDataReady();
    void initTimer();

private:
    void initialize();

private:
    QMutex mMutex;
    QWaitCondition mCondition;
    bool mAbort;
    QUdpSocket *mUdpSocketNavData;
    QList<QByteArray> mDatagramList;
    QTimer *mInitTimer;
    bool mDroneResponded;
    bool mDroneBooting;
};

#endif // NAVDATAHANDLER_H
