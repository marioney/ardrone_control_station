/*
Copyright (c) 2006-2009, Fred Emmott <mail@fredemmott.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#ifndef _JOYSTICK_H
#define _JOYSTICK_H

#include <QObject>
#include <QThread>
#include <QString>
#include <QMutex>
#include <QWaitCondition>
#include <SDL/SDL.h>
#include <QBasicTimer>
#include <QDebug>
#include <sys/syscall.h>
#include <QDateTime>

#define POLL_INTERVAL 20

class QBasicTimer;
class QTimerEvent;

class Joystick : public QThread
{
    Q_OBJECT
	public:
		Joystick(int jsNumber = 0);
		~Joystick();

		int attachedJoysticks();
		int currentJoystick();
		QString joystickName(int id);
    protected:
        void run();
  public Q_SLOTS:
		void setJoystick(int jsNumber);
  private Q_SLOTS:
                virtual void timerEvent(QTimerEvent* event);
  Q_SIGNALS:
        void sendData(qreal,qreal);
        void sendAxis(qreal,qreal);
        void changeCamera();
        void emergencyPressed();
        void startPressed();
        void saveImg();
	private:
        void buttonRelesed(int button);
        void buttonPressed(int button);
        void AxisModified(SDL_JoyAxisEvent);
        int jsNum;
        QBasicTimer* m_timer;
        SDL_Joystick* m_joystick;
        QMutex mMutex;
        QWaitCondition mCondition;
};

//defines the gamepad configuration
typedef enum {
  PAD_CAM = 0,
  PAD_GAZ_D,
  PAD_TAKEPIC,
  PAD_GAZ_U,
  PAD_YAW_R,
  PAD_YAW_L,
  PAD_AUX2,
  PAD_AUX3,
  PAD_EMRGENCY,
  PAD_START
} PAD_BUTTONS;
#endif
