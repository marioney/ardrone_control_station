/**
 * @file /include/prueba_qt/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef prueba_qt_QNODE_HPP_
#define prueba_qt_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <QThread>
#include <QStringListModel>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

//#include <cv_bridge/cv_bridge.h>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cvwimage.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>

#include <ardrone_control_station/navdatahandler.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace prueba_qt {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode : public QThread {
    Q_OBJECT
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
	bool init(const std::string &master_url, const std::string &host_url);
	void run();

	/*********************
	** Logging
	**********************/
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);
        IplImage* QImage2IplImage(QImage *qimg);

Q_SIGNALS:
	void loggingUpdated();
    void rosShutdown();

public Q_SLOTS:
    void rcvImage(QImage Img);
    void publishNavdata(telemetry_t *, coordinate_t *);

private:
	int init_argc;
    char** init_argv;
//    image_transport::ImageTransport it(nh);
    image_transport::Publisher image_pub;
    ros::Publisher telemetry_pub;
    QStringListModel logging_model;
    QImage imagen;
};

}  // namespace prueba_qt

#endif /* prueba_qt_QNODE_HPP_ */
