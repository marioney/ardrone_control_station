#ifndef MAINWINDOW_H
#define MAINWINDOW_H


// Features
//#define SAVE_MEASURES_LOG_FILE

#define USE_VIDEO_PROCESSOR
#define USE_JOYSTICK
#define USE_ROS_NODE

#define MAP_WIDTH 560
#define MAP_HEIGHT 420

#include <QtGui/QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QHash>
#include <QWaitCondition>
#include <QTcpSocket>
#include <QtCore/QCoreApplication>
#include <QDebug>
#include <QTimer>
#include <QHostInfo>
#include <QFtp>
#include <QFile>
#include <QFileDialog>
#include <QDomDocument>

#include <ardrone_control_station/threadcomport.h>
#include <ardrone_control_station/navdatahandler.h>
#include <ardrone_control_station/settings.h>
#include <ardrone_control_station/attitude_indicator.h>
#ifdef USE_ROS_NODE
#include <ardrone_control_station/qnode.h>
#endif

    #include "Joystick.h"
//#include "missioncontrol.h"
//#include <QWidget>

#include <sys/syscall.h>
#include <qwt/qwt_compass_rose.h>
#include <qwt/qwt_dial_needle.h>
#include <qmath.h>
#include <QProcess>


/*  NAD83	6,378,137.0	6,356,752.3141
    WGS84	6,378,137.0	6,356,752.3142
    Clark 1866	6,378,206.4	6,356,583.8
    Airy 1830	6,377,563.4	6,356,256.9*/

#define WGS84A 6378137.0
#define WGS84B 6356752.3142
//#define WGS84A 6378388.0
//#define WGS84B 6356911.946130
#define EX (sqrt((WGS84A*WGS84A) - (WGS84B*WGS84B)))/WGS84A //excentricidad
#define EX_ (sqrt((WGS84A*WGS84A) - (WGS84B*WGS84B)))/WGS84B//segunda excentricidad
#define EX_2 (EX_*EX_)
#define RP (WGS84A*WGS84A)/WGS84B //radio polar de curvatura c
#define ALFA (WGS84A-WGS84B)/WGS84A //Aplanamiento
#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164
#define RAD(x) ((x)*PI)/180

class NavDataHandler;
class AtCommand;
class VideoProcessor;
class missionCtrl;

class QHostInfo;
class QFtp;
class QFile;


//typedef struct _wayPoint_t
//{
//    int ID;
//    float yaw;
//    coordinate_t coordinate;
//    float altitude;
//} wayPoint_t;
//typedef struct _mission_t
//{
//    int wpID;
//    int photo;
//    int timeDelay;
//    int err;
//    wayPoint_t wayPoint;
//} mission_t;
//typedef struct _missionConf_t
//{
//    float yawErr,altErr,distWp,missionTout;
//    coordinate_t boundMapIni,boundMapEnd;
//} missionConf_t;

//enum MSG_KEYS
//{
//    ID_MSG = 1,         //RMC protocol header
//    UTC,                //UTC Time hhmmss.ss
//    STATUS,             //A=data valid or V=data not valid
//    LATITUDE,           //dddmm.mmmm
//    LAT_ORIENTATION,    //N=north or S=south
//    LONGITUDE,          //dddmm.mmmm
//    LON_ORIENTATION,    //E=east or W=west
//    SPEED,              //Speed Over Ground in knots
//    COURSE,             //Course Over Ground in degrees
//    DATE,               //ddmmyy
//    MAG_VAR,            //Magnetic Variation in degrees
//    M_V_ORIENTATION,    //E=east or W=west
//    MODE,
//    CHECKSUM            //checksum
//};

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
#ifdef USE_ROS_NODE
    explicit MainWindow(int argc, char** argv, QWidget *parent = 0);
#else
    explicit MainWindow(QWidget *parent = 0);
#endif
    virtual ~MainWindow();

protected:
    void paintEvent(QPaintEvent *event);
private:
    void setStatusLabelText(QString text);
    QString setErrorLabelText(QString text, QString size = "10");
    void uploadFile();
    QString formatStateLabel(int state,QString text0,QString text1,QString color0,QString color1);
    void calculateUTM(coordinate_t *);
    void showMission();
    void testCoordinates(QString,QString,QString,QString);

private Q_SLOTS:
    void on_followPatternBtn_clicked();
    void on_captureFramesBtn_clicked();
    void on_imgMapBtn_clicked();
    void on_chageCamBtn_clicked();
    void on_startMissionBtn_2_clicked();
    void on_FTrimBtn_clicked();
    void on_pushButton_clicked();
    void on_clearMissionBtn_clicked();
    void on_startMissionBtn_clicked();
    void on_loadMissionBtn_clicked();
    void on_buttonBox_accepted();
    void on_imgDirectoryBtn_clicked();
    void droneConnected(QString version);
    void noDroneConnected();

    void on_saveImageBtn_clicked();
    void on_ardroneConnectBtn_clicked();
    void on_settingsButton_clicked();
    void handleLoaderTimer();
    void lookUpHost();
    void lookedUp(const QHostInfo &host);
    void readDroneFtp();
    void droneConnectionTimeout();
//    void receiveMsg(const QTime timesl, const unsigned char *data, const int size);
    void receiveMsg(coordinate_t * curr);
    void on_emgResetBtn_clicked();
    void iwconfig();

public Q_SLOTS:
    void recieveVideoImage(QImage *image);
    void updateNavdata(telemetry_t *);
    void updateEmergencyState(bool on);
    void saveImg();
    void showCurrentStep(int);
    void newConfDataReady();

Q_SIGNALS:

    void mission(QList<mission_t> * ,missionConf_t *);
    void sendLocation( coordinate_t *);
    void camVert(bool);
#ifdef USE_ROS_NODE
    void PublishImg(QImage);
    void publishNavdata(telemetry_t *, coordinate_t *);
    void detectTag(int control);
#endif
private:
    NavDataHandler *mNavDataHandler;
    AtCommand *mAtCommand;
//    missionCtrl *mMissionCtrl;
#ifdef USE_VIDEO_PROCESSOR
    VideoProcessor *mVideoProcessor;
#endif
    Joystick *mJoystick;
    bool mNoConnectionExit,mConnectedToDrone,homeSet,toutFlag,isMissionLoaded, isHomesetted,paintFirstTime,newGpsData, recFrames;
    Ui::MainWindow *ui;
    QTimer *mLoaderTimer;
    int mLoadDots;
    QFtp *mFtp;
    QString mVersion,imgDirPath,portName,userName;
    uint mConnectState;
    QGraphicsScene *scene, *sceneMap;
    QImage lastImage;
    Settings *msettings;
    QIcon *icono;
    QMessageBox msgBox;
    AttitudeIndicator *test;
    Qthreadcomport *port;
    QMainComThread *mainComThread;
    coordinate_t homeCoord,currCoord;
    mission_t paso_actual;
    missionConf_t confg;
    uint lastState;
    QList<mission_t> missionList;
    QMutex mMutex;
    QWaitCondition mCondition;

    float correccionPsi;
    bool takePsiOffset;
    QPixmap *terrain;
#ifdef SAVE_MEASURES_LOG_FILE
    QFile measurementLog;
    QByteArray lastCommand;
#endif
    QTcpSocket *tcpSocketCmd;
    QProcess *myProcess;
#ifdef USE_ROS_NODE
    prueba_qt::QNode qnode;
#endif
};

#endif // MAINWINDOW_H
