#include <ardrone_control_station/mainwindow.h>

#include <QtGui>
#include<QApplication>

int main(int argc, char *argv[])
{
    QApplication::setGraphicsSystem("raster");
    QApplication app(argc, argv);
    MainWindow mainWindow(argc,argv);
    mainWindow.setGeometry(0,0,1240,740);
    mainWindow.showNormal();
    mainWindow.setVisible(false);
    return app.exec();
}
