#include <ardrone_control_station/Joystick.h>



Joystick::Joystick(int js)
{
    jsNum=js;
    qDebug() << "[Joystick] Starting thread";
    QMutexLocker locker(&mMutex);
    if (!isRunning()) {
        start(HighPriority);
    } else {
        mCondition.wakeOne();
    }

}

Joystick::~Joystick()
{
        delete m_timer;
        SDL_JoystickClose(m_joystick);
        SDL_QuitSubSystem(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
}

void Joystick::run()
{
//    qDebug() << "[Joystick] run: "<<(pid_t) syscall (SYS_gettid);
    // Sure, we're only using the Joystick, but SDL doesn't work if video isn't initialised
        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);

//        Q_ASSERT(js < attachedJoysticks());
//        Q_ASSERT(js >= 0);

        m_joystick = SDL_JoystickOpen(jsNum);

        m_timer = new QBasicTimer();
        m_timer->start(POLL_INTERVAL, this);
}

int Joystick::currentJoystick()
{
	return SDL_JoystickIndex(m_joystick);
}

QString Joystick::joystickName(int js)
{
//	Q_ASSERT(js < attachedJoysticks());
//	Q_ASSERT(js >= 0);
	return QString(SDL_JoystickName(js));
}

void Joystick::setJoystick(int js)
{
//	Q_ASSERT(js < attachedJoysticks());
//	Q_ASSERT(js >= 0);

	SDL_JoystickClose(m_joystick);
	m_joystick = SDL_JoystickOpen(js);
}



void Joystick::timerEvent(QTimerEvent*)
{
	SDL_Event event;

	m_timer->stop();
	while ( SDL_PollEvent(&event) )
	{
		switch(event.type)
		{
		case SDL_JOYAXISMOTION:
            AxisModified(event.jaxis);
			break;
		case SDL_JOYBUTTONDOWN:
            buttonPressed(event.jbutton.button);
			break;
		case SDL_JOYBUTTONUP:
            buttonRelesed(event.jbutton.button);
			break;
		}
	}
	m_timer->start(POLL_INTERVAL, this);
}

int Joystick::attachedJoysticks()
{
	return SDL_NumJoysticks();
}
void Joystick::buttonRelesed(int button)
{
//    qDebug() << button << "was released";
    switch(button){
        case PAD_CAM:
            break;
        case PAD_GAZ_D:
            Q_EMIT sendData(0.0, 0.0);
            break;
        case PAD_GAZ_U:
            Q_EMIT sendData(0.0, 0.0);
            break;
        case PAD_YAW_R:
            Q_EMIT sendData(0.0, 0.0);
            break;
        case PAD_YAW_L:
            Q_EMIT sendData(0.0,0.0);
            break;
        case PAD_EMRGENCY:
            break;
        case PAD_START:
            break;
        case PAD_TAKEPIC:
            break;
        default:
            break;
    }
}
void Joystick::buttonPressed(int button)
{
    //    qDebug() << button << "was pressed";
    switch(button){
        case PAD_CAM:
            Q_EMIT changeCamera();
            break;
        case PAD_GAZ_D:
            Q_EMIT sendData(-1.0, 0.0);
            break;
        case PAD_GAZ_U:
            Q_EMIT sendData(1.0, 0.0);
            break;
        case PAD_YAW_R:
            Q_EMIT sendData(0.0,-1.0);
            break;
        case PAD_YAW_L:
            Q_EMIT sendData(0.0,1.0);
            break;
        case PAD_EMRGENCY:
            Q_EMIT emergencyPressed();
            break;
        case PAD_START:
                Q_EMIT startPressed();
            break;
        case PAD_TAKEPIC:
                Q_EMIT saveImg();
            break;
        default:
            break;
    }
}
void Joystick::AxisModified(SDL_JoyAxisEvent axis)
{
    //    qDebug() << axis.axis << "in X and " << axis.value << "in Y";
        qreal phi=0.0, theta=0.0;
        qreal h = axis.value/32767.0;
    //    qDebug() << value << "fixed at: " << h;
        if (h < 0.1 && h > -0.1) {
            h = 0.0;
        } else if (h > 1.0) {
            h = 1.0;
        } else if (h < -1.0) {
            h = -1.0;
        }
        if (0 == axis.axis) {
            phi = h;
        }
        if (1 == axis.axis){
            theta = h;
        }
//        qDebug() << QDateTime::currentDateTime().toString("ss.zzz") << " " << axis.axis << " " << axis.value;
        Q_EMIT sendAxis(phi,theta);
}
