#include <ardrone_control_station/missioncontrol.h>

/*!
    Constructs the \c MissionCtrl.
    The \a parent parameter is is sent to the QThread constructor.
*/
missionCtrl::missionCtrl(AtCommand *mAtCmd, QObject *parent) : QThread(parent)
{
    qDebug() << "[missionCtrl] Constructor";
    mAtCommand= mAtCmd;

}

/*!
    Destructor.
*/
missionCtrl::~missionCtrl()
{
    qDebug() << "[missionCtrl] Destructor";
    abortMssn=true;
    this->wait(1000);
}

/*!
    Starts the thread with \c HighPriority unless it is already running.
*/
void missionCtrl::startThread()
{
    qDebug() << "[missionCtrl] Starting thread";
    QMutexLocker locker(&mMutex);
    if (!isRunning()) {
        start(NormalPriority);
//        start(TimeCriticalPriority);
    } else {
        mCondition.wakeOne();
    }
}

/*!
    Thread main function.
*/
void missionCtrl::run()
{
//    qDebug() << "[missionCtrl] run: "<<(pid_t) syscall (SYS_gettid);
//    emit showCurrentStep(0);
//    if(missionList.size()==0)
//    {
//        return;
//    }
//    abortMssn=false;
//    emit startPressed();
//    this->sleep(3);
//    emit sendData(1,0);
//    sleep(2);
//    for (int i=0;i<missionList.size();i++)
//    {
//        if(abortMssn){
//            emit sendData(0,0);
//            emit sendAxis(0,0);
//            qDebug()<<"Break desde el while de paso";
//            return;
//        }
//        paso_actual=missionList.at(i);
//        emit showCurrentStep(i);
//        qDebug()<<"paso actual: "<<i<<paso_actual.timeDelay;
//        bool yawOk=false;
//        bool gpsOk=false;
//        toutFlag =true;
//        QTimer::singleShot(20000,this,SLOT(handletout()));
//
//            float errYaw, errAlt, errX, errY;
//        while (!gpsOk)
//        {
//            if(abortMssn){
//                emit sendData(0,0);
//                emit sendAxis(0,0);
//                qDebug()<<"Break desde el while de gps";
//                return;
//            }
//            float Ang = calculateVector();
//            qreal errAng = Ang - currTelemetry.tmpPsi/1000;
//            if(errAng>180) errAng -= 360;
//            if(errAng<-180) errAng += 360;
////            qDebug()<<currTelemetry.tmpPsi/1000<<Ang<<errAng;
//            if (fabs(errAng)<=config.yawErr)
//            {
////                sendData(0,0);
//                errX=fabs(currCoord.x - paso_actual.wayPoint.coordinate.x);
//                errY=fabs(currCoord.y - paso_actual.wayPoint.coordinate.y);
//
////                qDebug()<<QString::number(currCoord.x, 'f', 6 )<<QString::number(paso_actual.wayPoint.coordinate.x, 'f', 6 )
////                        <<QString::number(currCoord.y, 'f', 6 )<<QString::number(paso_actual.wayPoint.coordinate.y, 'f', 6 );
////                qDebug()<<mag<<paso_actual.err;
//                if(mag<=paso_actual.err){
////                if(errX<=paso_actual.err && errY<=paso_actual.err){
//                    qDebug()<<"Waypoint "<<i<<" Alcanzado";
//                    gpsOk=true;
//                }else{
//                    emit sendAxis(0,-.1);
//                    msleep(100);
//                    emit sendAxis(0,0);
//                }
//
//            }else{
//                emit sendData(0,errAng/180);
//            }
//            this->msleep(50);
//        }
//            //este ciclo ajusta el angulo y la altura
//        while (!yawOk)
//        {
//
//            if(abortMssn){
//                emit sendData(0,0);
//                emit sendAxis(0,0);
//                qDebug()<<"Break desde el while de yaw";
//                return;
//            }
//            errYaw= paso_actual.wayPoint.yaw - currTelemetry.tmpPsi/1000;
//            if(errYaw>180) errYaw -= 360;
//            if(errYaw<-180) errYaw += 360;
//            errAlt= paso_actual.wayPoint.altitude - currTelemetry.tmpAltitude;
////            qDebug()<<currTelemetry.tmpPsi/1000<<paso_actual.wayPoint.yaw<<errYaw;
////            qDebug()<<currTelemetry.tmpAltitude<<paso_actual.wayPoint.altitude<<errAlt;
//            if (fabs(errYaw)<=config.yawErr && fabs(errAlt)<=config.altErr)
//            {
//                emit sendData(0,0);
//                if(paso_actual.photo>0){
//                    switch (paso_actual.photo){
//                    case 1:
//                        if(!cam) {
//                            emit changeCamera();
//                            msleep(500);
//                            break;
//                        }
//                    case 2:
//                        if(cam){
//                            emit changeCamera();
//                            msleep(500);
//                            break;
//                        }
//                    default:
//                        break;
//                    }
//                    emit saveImg();
//                    emit changeCamera();
//                    this->sleep(paso_actual.timeDelay);
//                }
//
//                yawOk=true;
//            }
//            else{
//                emit sendData(errAlt/1000,errYaw/180);
//            }
//            this->msleep(60);
//        }
//
//
//        toutFlag=false;
//
//        QTimer::singleShot(paso_actual.timeDelay*1000,this,SLOT(saveImg()));
////
//    }
//    this->sleep(1);
//    emit stopPressed();
    Q_FOREVER {
        if (abortMssn) {
            Q_EMIT sendData(0,0);
            return;
        }
        if(currTelemetry.nb_detected==1)
        {
            qreal yaw = -(500.0-currTelemetry.xc)/500.0;
            if(yaw > 1.0)
                yaw = 1.0;
            else if(yaw < -1.0)
                yaw = 1.0;
            qreal gaz = (500.0-currTelemetry.yc)/500.0;
            if(gaz > 1.0)
                gaz = 1.0;
            else if(gaz < -1.0)
                gaz = -1.0;
//            qDebug()<< "[MissionCtrl]"+QString::number(gaz,'f',3) + " , "+ QString::number(yaw,'f',3);
            Q_EMIT sendData(gaz,yaw);
        }
        else{
            Q_EMIT sendData(0,0);
        }
        this->msleep(60);

    }

}

void missionCtrl::handletout()
{
    if(toutFlag) {
        abortMssn=true;
    }
}
qreal missionCtrl::calculateVector(){
    double a=paso_actual.wayPoint.coordinate.x - currCoord.x;
    double b=paso_actual.wayPoint.coordinate.y - currCoord.y;
    mag = sqrt(((a)*(a))+((b)*(b)));
    qreal angle= 90-atan2(b,a)*180/PI; //angulo deseado
    if(angle>180) angle -= 360;

//    qreal psiCtrl=currTelemetry.tmpPsi/1000 - angle; //angulo que debe girar
//    if(psiCtrl>180) psiCtrl -= 360;
//    qreal mPsi = (RAD(psiCtrl)/YAW_MAX)*-1;
//
//    qDebug()<<mag<<" "<<angle<<" "<<psiCtrl<<" "<<mPsi;
    return angle;
}

void missionCtrl::getTelemetry(telemetry_t *tlmtry){
    currTelemetry= *tlmtry;
}
void missionCtrl::getMission(QList<mission_t> *msnWp ,missionConf_t *conf)
{
    config = *conf;
    missionList = *msnWp;
}
void missionCtrl::getCurrLocation(coordinate_t *currloc)
{
    currCoord = *currloc;
}
void missionCtrl::abortMission(int button)
{
    abortMssn=true;
    qDebug()<<"aborted by: "+ button;
}

void missionCtrl::camVert(bool currCam){
    cam=currCam;
}
